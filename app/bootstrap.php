<?php

//LIMPAR O OB
ob_start();

//INICIAR SESSÃO
session_start();

//REQUIRE DO CONFIG
require_once 'config/config.php';

//REQUIRE DOS HELPERS DE CLASSES
require_once 'helpers/classes/vendor/autoload.php';

//REQUIRE DOS HELPERS DE FUNÇÕES
require_once 'helpers/functions/url_helper.php';
require_once 'helpers/functions/nome_helper.php';
require_once 'helpers/functions/cpf_helper.php';
require_once 'helpers/functions/flash_helper.php';
require_once 'helpers/functions/texto_helper.php';
require_once 'helpers/functions/mailsender_helper.php';

//AUTO-CARREGAR AS BIBLIOTECAS
spl_autoload_register(function ($className) {

    $arquivo = 'libraries' . DIR_STR . $className . '.php';

    if (file_exists(APP_ROOT . DIR_STR . $arquivo)) {
        require_once $arquivo;
    }
});



