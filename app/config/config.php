﻿<?php

//CONFIGURAÇÕES DO BANCO DE DADOS
define("DB_HOST", "localhost");
define("DB_NAME", "siscard");
define("DB_USER", "root");
define("DB_PASS", "");

//URL ROOT
define('URL_ROOT', 'http://siscard.test');

//INFORMAÇÕES & CONFIGURAÇÕES DO SITE
define('SITE_NAME', 'Siscard'); //NOME DO SITE
define('PRECO_UNITARIO', 1.08);

/*
 * NÃO ALTERAR AS CONFIGURAÇÕES ABAIXO!
 */

//SEPARADOR PADRÃO DE ACORDO COM O SISTEMA OPERACIONAL
define('DIR_STR', DIRECTORY_SEPARATOR);

//APP ROOT
define('APP_ROOT', dirname(dirname(__FILE__)));

//PUBLIC ROOT
define('PUBLIC_ROOT', dirname(dirname(dirname(__FILE__))) . DIR_STR . 'public');

//UPLOAD ROOT
define('UPLOAD_ROOT', PUBLIC_ROOT . DIR_STR . 'uploads');

//DEFINIR TIMEZONE PADRÃO
date_default_timezone_set('America/Fortaleza');
