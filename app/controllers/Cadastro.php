<?php

class Cadastro extends Controller
{

    public $viewData = array();

    public function __construct()
    {
        //CARREGAR OS MODELS
        $this->alunoModel = $this->model('Aluno');
        $this->cursoModel = $this->model('Curso');
        $this->sessaoModel = $this->model('Sessao');
        $this->matriculaModel = $this->model('Matricula');
        $this->fotoModel = $this->model('Foto');

        //PRÉ-CARREGAR VALORES QUE SERÃO PASSADOS EM TODAS AS VIEWS
        $this->viewData = [
            'tituloDaPagina' => '',
            'listaDeCursos' => $this->cursoModel->listarTodos(),
            'formData' => [],
            'formError' => [],
        ];
    }

    public function index()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $matricula = filter_input(INPUT_POST, 'matricula', FILTER_SANITIZE_NUMBER_INT);

            //VALIDAR MATRÍCULA
            switch (true) {
                case empty($matricula):
                    $this->viewData['formError']['matricula'] = 'Digite o número da matrícula!';
                    break;
                case strlen($matricula) != 6:
                    $this->viewData['formError']['matricula'] = 'A matrícula deve ter 6 digitos!';
                    break;
                case $this->matriculaModel->existeMatricula($matricula) == FALSE:
                    $this->viewData['formError']['matricula'] = 'N° da matrícula inválida!';
                    break;
                case $this->alunoModel->verificarMatricula($matricula):
                    $this->viewData['formError']['matricula'] = 'Essa matrícula já tem cadastro no Siscard!';
                    break;
                default:
                    $this->viewData['formError']['matricula'] = '';
            }

            if (!array_filter($this->viewData['formError'])) {
                redirect('cadastro/formulario/' . $matricula);
            }
        }

        $this->viewData['tituloDaPagina'] = 'Formulário de Pré-cadastro';
        $this->view('pages/precadastro', $this->viewData);
    }

    public function formulario($matricula = '')
    {

        $matricula = filter_var($matricula, FILTER_SANITIZE_NUMBER_INT);

        //VERIFICAR SE A MATRÍCULA EXISTE
        if ($this->matriculaModel->existeMatricula($matricula) == FALSE) {
            redirect('cadastro');
        } else if ($this->alunoModel->verificarMatricula($matricula) == TRUE) {
            flash('precadastro', 'Ops! Essa matrícula já tem cadastro no Siscard!', 'alert alert-danger');
            redirect('cadastro');
        } else if ($this->matriculaModel->cadastrosAbertos($matricula) == FALSE) {
            flash('precadastro', 'Os cadastros estão desativados para seu curso!', 'alert alert-danger');
            redirect('cadastro');
        } else {

            //PEGAR TODOS OS DADOS DA MATRÍCULA INFORMADA
            $dadosMatricula = $this->matriculaModel->dadosMatricula($matricula);

            //VERIFICAR SE HOUVE UM REQUEST DE POST
            if (filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING) == 'POST') {
                $nascimento = filter_input(INPUT_POST, 'nascimento', FILTER_SANITIZE_STRING);
                $cpf = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_NUMBER_INT);
                $rg = filter_input(INPUT_POST, 'rg', FILTER_SANITIZE_NUMBER_INT);
                $senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING);
                $csenha = filter_input(INPUT_POST, 'csenha', FILTER_SANITIZE_STRING);
                $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);

                /*
                 * VALIDAÇÃO DOS DADOS
                 */

                //VALIDAÇÃO DE NASCIMENTO
                switch (true) {
                    case empty($nascimento):
                        $this->viewData['formError']['nascimento'] = 'Digite sua data de nascimento!';
                        break;
                    case preg_match('/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/', $nascimento) == 0:
                        $this->viewData['formError']['nascimento'] = 'Data de nascimento inválida!';
                        break;
                    default:
                        $this->viewData['formError']['nascimento'] = '';
                        $this->viewData['formData']['nascimento'] = $nascimento;
                }

                //VALIDACAO CPF
                switch (true) {
                    case empty($cpf):
                        $this->viewData['formError']['cpf'] = 'Digite o n° do seu CPF!';
                        break;
                    case!is_numeric($cpf):
                        $this->viewData['formError']['cpf'] = 'N° do CPF inválido!';
                        break;
                    case!validarCPF($cpf):
                        $this->viewData['formError']['cpf'] = 'N° do CPF inválido!';
                        break;
                    case $this->alunoModel->verificarCPF($cpf) AND $cpf != $this->viewData['dadosAluno']->cpf:
                        $this->viewData['formError']['cpf'] = 'Já existe um cadastro com esse CPF!';
                        break;
                    default:
                        $this->viewData['formError']['cpf'] = '';
                        $this->viewData['formData']['cpf'] = $cpf;
                }

                //VALIDACAO DO RG
                switch (true) {
                    case empty($rg):
                        $this->viewData['formError']['rg'] = 'Digite o n° do seu RG!';
                        break;
                    case!is_numeric($rg):
                        $this->viewData['formError']['rg'] = 'N° do RG inválido!';
                        break;
                    case strlen($rg) < 4:
                        $this->viewData['formError']['rg'] = 'N° do RG inválido!';
                        break;
                    case $this->alunoModel->verificarRG($rg) AND $rg != $this->viewData['dadosAluno']->rg:
                        $this->viewData['formError']['rg'] = 'Já existe um cadastro com esse RG!';
                        break;
                    case $rg == $cpf:
                        $this->viewData['formError']['rg'] = 'O RG não pode ser igual ao CPF!';
                        break;
                    default:
                        $this->viewData['formError']['rg'] = '';
                        $this->viewData['formData']['rg'] = $rg;
                }

                //VALIDAÇÃO DE SENHA
                switch (true) {
                    case empty($senha):
                        $this->viewData['formError']['senha'] = 'Digite sua senha!';
                        break;
                    case strlen($senha) < 8:
                        $this->viewData['formError']['senha'] = 'A senha deve ter mais de 8 caracteres!';
                        break;
                    default:
                        $this->viewData['formError']['senha'] = '';
                        $this->viewData['formData']['senha'] = $senha;
                        break;
                }

                //VALIDAÇÃO DE CONFIRMAÇÃO DE SENHA
                switch (true) {
                    case empty($csenha):
                        $this->viewData['formError']['csenha'] = 'Confirme sua senha!';
                        break;
                    case $senha != $csenha:
                        $this->viewData['formError']['csenha'] = 'A senha informada não confere!';
                        break;
                    default:
                        $this->viewData['formError']['csenha'] = '';
                        $this->viewData['formData']['csenha'] = $csenha;
                        break;
                }

                //VALIDACAO DE EMAIL
                switch (true) {
                    case empty($email):
                        $this->viewData['formError']['email'] = 'Digite seu e-mail!';
                        break;
                    case!filter_var($email, FILTER_VALIDATE_EMAIL):
                        $this->viewData['formError']['email'] = 'O e-mail informado é inválido!';
                        break;
                    case $this->alunoModel->verificarEmail($email):
                        $this->viewData['formError']['email'] = 'Já existe um cadastro com esse e-mail!';
                        break;
                    default:
                        $this->viewData['formError']['email'] = '';
                        $this->viewData['formData']['email'] = $email;
                        break;
                }

                //VERIFICAR SE NÃO HOUVE ERROS / SE NÃO HOUVER, TENTAR REALIZAR CADASTRO
                if (!array_filter($this->viewData['formError'])) {
                    //SEPARAR OS DADOS QUE SERÃO USADOS NO CADASTRO
                    $nome = $dadosMatricula->nome;
                    $cursid = $dadosMatricula->cursid;

                    //REALIZAR CADASTRO E MANDAR MENSAGEM PARA A VIEW
                    $addAluno = $this->alunoModel->cadastrarAluno(['nome' => $nome, 'matricula' => $matricula, 'nascimento' => $nascimento, 'cpf' => $cpf, 'rg' => $rg, 'cursid' => $cursid, 'senha' => $senha, 'email' => $email]);
                    if ($addAluno) {
                        //ADD PROGRESSO
                        $this->alunoModel->addProgressoAluno('Cadastro realizado!', 'Você realizou seu cadastro, agora envie uma foto para finalizá-lo!', 'cadastrorealizado', $addAluno);
                        //ADD FOTO
                        $this->fotoModel->addFoto($addAluno);

                        //ENVIAR O E-MAIL COM OS DADOS DO ALUNO
                        $mailer = new MailSender;
                        $mailer->setTemplateURL(PUBLIC_ROOT . DIR_STR . 'assets' . DIR_STR . 'email' . DIR_STR . 'boas-vindas.html');
                        $mailer->compose(array(
                            'urlPath' => URL_ROOT . '/public/assets/email/',
                            'nomeAluno' => $nome,
                            'matriculaAluno' => $matricula,
                            'emailAluno' => $email,
                            'senhaAluno' => $senha
                        ));
                        $enviar = $mailer->send(array('contato@includejr.com.br', 'Siscard'), $email, 'Seus dados cadastrais no Siscard');

                        //REALIZAR LOGIN DO ALUNO
                        $this->sessaoModel->apagarSessao('aluno_passe');
                        $this->sessaoModel->novaSessao('aluno_passe', $addAluno);
                        $this->sessaoModel->novaSessao('boas_vindas', true);
                        redirect('painel/index');
                    } else {
                        flash('cadastro', 'Houve um erro durante o seu cadastro!', 'alert alert-danger');
                    }
                }
            }

            $this->viewData['dadosMatricula'] = $dadosMatricula;
            $this->viewData['tituloDaPagina'] = 'Formulário de Cadastro';
            $this->view('pages/cadastro', $this->viewData);
        }
    }

}
