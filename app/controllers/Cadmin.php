<?php

class Cadmin extends Controller
{

    public $viewData = array();

    public function __construct()
    {
        //CARREGAR OS MODELS
        $this->caModel = $this->model('Ca');
        $this->alunoModel = $this->model('Aluno');
        $this->cursoModel = $this->model('Curso');
        $this->sessaoModel = $this->model('Sessao');

        //PRÉ-CARREGAR VALORES QUE SERÃO PASSADOS EM TODAS AS VIEWS
        $this->viewData = [
            'tituloDaPagina' => '',
            'formData' => [],
            'formError' => [],
        ];
    }

    public function index()
    {
        //DEFINIR TITULO DA PÁGINA
        $this->viewData['tituloDaPagina'] = 'Área de Login do CA';

        if (filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING) == 'POST') {

            //VARIAVEIS POST
            $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
            $senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING);
            $ca_lembrarme = filter_input(INPUT_POST, 'ca_lembrarme');

            //VALIDACAO DE EMAIL
            switch (true) {
                case empty($email):
                    $this->viewData['formError']['email'] = 'Digite seu e-mail!';
                    break;
                case!filter_var($email, FILTER_VALIDATE_EMAIL):
                    $this->viewData['formError']['email'] = 'O e-mail informado é inválido!';
                    break;
                case!$this->caModel->verificarEmail($email):
                    $this->viewData['formError']['email'] = 'O e-mail informado não existe!';
                    break;
                default:
                    $this->viewData['formError']['email'] = '';
                    $this->viewData['formData']['email'] = $email;
                    break;
            }

            //VALIDAR A SENHA
            switch (true) {
                case empty($senha):
                    $this->viewData['formError']['senha'] = 'Digite sua senha!';
                    break;
                case $this->caModel->verificarEmail($email) AND !password_verify($senha, $this->caModel->meusDados($email)->senha):
                    $this->viewData['formError']['senha'] = 'A senha informada não confere!';
                    break;
                default:
                    $this->viewData['formError']['senha'] = '';
                    break;
            }

            //VERIFICAR SE A PESSOA MARCOU LEMBRAR-ME NO FORMULÁRIO E SALVAR O PASSE EM UMA SESSÃO
            if ($ca_lembrarme == 'on') {
                $_SESSION['ca_lembrarme'] = $email;
            }

            //VERIFICAR SE NÃO HOUVE ERROS / SE NÃO HOUVER, TENTAR REALIZAR CADASTRO
            if (!array_filter($this->viewData['formError'])) {
                $this->sessaoModel->novaSessao('cadmin_email', $email);
                redirect('capainel/index');
            }
        }
        $this->view('cadmin/login', $this->viewData);
    }

}
