<?php

//CHART
use ChartJs\ChartJS;
//PAGINATOR
use Intervention\Image\Exception\NotReadableException;
use JasonGrimes\Paginator;
//INTERVENTION IMAGE
use Intervention\Image\ImageManagerStatic as Image;

class Capainel extends Controller
{

    public $viewData = array();
    public $caAdminEmail;
    public $meuCurso;

    public function __construct()
    {
        //CARREGAR OS MODELS
        $this->caModel = $this->model('Ca');
        $this->alunoModel = $this->model('Aluno');
        $this->cursoModel = $this->model('Curso');
        $this->sessaoModel = $this->model('Sessao');
        $this->fotoModel = $this->model('Foto');
        $this->carteiraModel = $this->model('Carteira');

        $this->matriculaModel = $this->model('Matricula');
        $this->loteModel = $this->model('Lote');

        //DEFINIR EMAIL DO CA
        $this->caAdminEmail = $_SESSION['cadmin_email'];

        //VERIFICAR SE O ALUNO ESTÁ LOGADO, CASO CONTRÁRIO REDIRECIONÁ-LO PARA O LOGIN
        if ($this->sessaoModel->existeSessao('cadmin_email')) {
            if ($this->caModel->verificarEmail($this->caAdminEmail)) {
                if ($this->caModel->tenhoCursoAssociado($this->caAdminEmail) == FALSE) {
                    flash('cadmin_login', 'Essa conta ainda não tem um curso associado!', 'alert alert-danger mt-4');
                    redirect('cadmin');
                }
            } else {
                $this->sessaoModel->apagarSessao('cadmin_email');
                redirect('cadmin');
            }
        } else {
            redirect('cadmin');
        }

        //DEFINIR CURSO DO ADMIN
        $this->meuCurso = $this->caModel->meuCurso($this->caAdminEmail);

        //PRÉ-CARREGAR VALORES QUE SERÃO PASSADOS EM TODAS AS VIEWS
        $this->viewData = [
            'tituloDaPagina' => '',
            'formData' => [],
            'formError' => [],
            'meusDados' => $this->caModel->meusDados($this->caAdminEmail),
            'infoMeuCurso' => $this->caModel->infoMeuCurso($this->caAdminEmail),
            'totalFotosParaValidar' => $this->fotoModel->totalFotosValidacao($this->meuCurso),
        ];
    }

    public function index()
    {

        //LABELS DOS GRAFICOS
        $data = [
            'labels' => ['Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            'datasets' => [],
        ];

        //CORES DOS GRÁGICOS
        $colors = [
            ['backgroundColor' => 'rgba(12, 153, 230, 0.2)', 'borderColor' => 'rgba(12,153,230,1)'],
        ];

        //OPÇÕES DOS GRÁFICOS
        $options = ['responsive' => true];

        //ATRIBUTOS DOS GRÁFICOS
        $attributes = ['id' => '', 'width' => 380, 'height' => 250, 'style' => 'display:inline;'];

        //DATASETS
        $dataSetCarteiras = json_decode(json_encode(array_values($this->carteiraModel->carteirasGeradasPorMes($this->meuCurso))));
        $dataSetAlunos = json_decode(json_encode(array_values($this->alunoModel->alunosPorMes($this->meuCurso))));
        $datasets = [
            ['data' => $dataSetCarteiras, 'label' => "Total de Carteiras Geradas"] + $colors[0],
            ['data' => $dataSetAlunos, 'label' => "Total de Alunos Cadastrados"] + $colors[0],
        ];

        //CHART DAS CARTEIRINHAS
        $attributes['id'] = 'chartCarteirinhas';
        $chartCarteirinhas = new ChartJS('line', $data, $options, $attributes);
        $chartCarteirinhas->addDataset($datasets[0]);

        //CHART DOS ALUNOS
        $attributes['id'] = 'chartAlunos';
        $chartAlunos = new ChartJS('bar', $data, $options, $attributes);
        $chartAlunos->addDataset($datasets[1]);

        //MANDAR OS CHARTS PARA AS VIEWS
        $this->viewData['chartCarteirinhas'] = $chartCarteirinhas;
        $this->viewData['chartAlunos'] = $chartAlunos;

        $this->viewData['tituloDaPagina'] = 'Painel Administrativo do CA';
        $this->viewData['cards'] = [
            'totalMatriculasCurso' => $this->matriculaModel->totalMatriculasCurso($this->meuCurso),
            'totalAlunosCadastrados' => $this->alunoModel->totalAlunosCadastrados($this->meuCurso),
            'totalCarteirasGeradas' => $this->carteiraModel->totalCarteirasGeradas($this->meuCurso),
            'statusCadastro' => $this->cursoModel->statusCadastro($this->meuCurso),
        ];
        $this->view('cadmin/capainel', $this->viewData);
    }

    //CONFIGURAÇÕES
    public function configs()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $operacao = filter_input(INPUT_POST, 'operacao', FILTER_SANITIZE_NUMBER_INT);

            $meusDados = $this->caModel->meusDados($this->caAdminEmail);

            if ($operacao == 1) {
                //VARIAVEIS POST
                $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);

                //VALIDACAO DE EMAIL
                switch (true) {
                    case empty($email):
                        $this->viewData['formError']['email'] = 'Digite seu e-mail!';
                        break;
                    case !filter_var($email, FILTER_VALIDATE_EMAIL):
                        $this->viewData['formError']['email'] = 'O e-mail informado é inválido!';
                        break;
                    case $email != $meusDados->email and $this->caModel->verificarEmail($email):
                        $this->viewData['formError']['email'] = 'Já existe um cadastro com esse e-mail!';
                        break;
                    default:
                        $this->viewData['formError']['email'] = '';
                        break;
                }

                //VERIFICAR SE NÃO HOUVE ERROS / SE NÃO HOUVER, TENTAR ATUALIZAR E-MAIL
                if (!array_filter($this->viewData['formError'])) {
                    if ($this->caModel->atualizarEmail($meusDados->id, $email)) {
                        $this->sessaoModel->atualizarSessao('cadmin_email', $email);
                        flash('configs', 'E-mail atualizado com sucesso!');
                    } else {
                        flash('configs', 'Houve um erro na atualização do e-mail!', 'alert alert-danger');
                    }
                }
            } else if ($operacao == 2) {

                $senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING);
                $csenha = filter_input(INPUT_POST, 'csenha', FILTER_SANITIZE_STRING);

                //VALIDAÇÃO DE SENHA
                switch (true) {
                    case empty($senha):
                        $this->viewData['formError']['senha'] = 'Digite sua senha!';
                        break;
                    case strlen($senha) < 8:
                        $this->viewData['formError']['senha'] = 'A senha deve ter mais de 8 caracteres!';
                        break;
                    default:
                        $this->viewData['formError']['senha'] = '';
                        break;
                }

                //VALIDAÇÃO DE CONFIRMAÇÃO DE SENHA
                switch (true) {
                    case empty($csenha):
                        $this->viewData['formError']['csenha'] = 'Confirme sua senha!';
                        break;
                    case $senha != $csenha:
                        $this->viewData['formError']['csenha'] = 'A senha informada não confere!';
                        break;
                    default:
                        $this->viewData['formError']['csenha'] = '';
                        break;
                }

                //VERIFICAR SE NÃO HOUVE ERROS / SE NÃO HOUVER, TENTAR ATUALIZAR SENHA
                if (!array_filter($this->viewData['formError'])) {
                    if ($this->caModel->atualizarSenha($meusDados->id, password_hash($senha, PASSWORD_DEFAULT))) {
                        flash('configs', 'Senha atualizada com sucesso!');
                    } else {
                        flash('configs', 'Houve um erro na atualização da senha!', 'alert alert-danger');
                    }
                }
            } else if ($operacao == 3) {
                $cadastros = filter_input(INPUT_POST, 'cadastros', FILTER_SANITIZE_NUMBER_INT);

                //VALIDAÇÃO DO CHECKBOX DE CADASTROS
                switch (true) {
                    case is_null($cadastros):
                        $this->viewData['formError']['cadastros'] = 'Selecione uma opção!';
                        break;
                    case $cadastros < 0 or $cadastros > 1:
                        $this->viewData['formError']['cadastros'] = 'Opção inválida!';
                        break;
                    default:
                        $this->viewData['formError']['cadastros'] = '';
                        break;
                }

                //VERIFICAR SE NÃO HOUVE ERROS / SE NÃO HOUVER, TENTAR ATUALIZAR O STATUS DOS CADASTROS
                if (!array_filter($this->viewData['formError'])) {
                    if ($this->cursoModel->atualizarCadastro(['status' => $cadastros, 'id' => $this->meuCurso])) {
                        flash('configs', 'O status do cadastro foi atualizado com sucesso!');
                    } else {
                        flash('configs', 'Houve um erro na atualização do status do cadastro!', 'alert alert-danger');
                    }
                }
            } else {
                redirect('capainel/configs');
            }
        }

        $this->viewData['tituloDaPagina'] = 'Configurações';
        $this->view('cadmin/configs', $this->viewData);
    }

    //GERIR ALUNOS
    public function geriralunos($pagina = '')
    {

        /*
         * PAGINAÇÃO
         */

        //CONFIGURAÇÕES DA PAGINAÇÃO
        $totalRegistros = $this->alunoModel->totalAlunosCurso($this->meuCurso);
        $registrosPorPagina = 30;
        $padraoUrl = URL_ROOT . '/capainel/geriralunos/(:num)';

        //VALIDAR A PÁGINA
        if (empty($pagina) or is_numeric($pagina) == FALSE) {
            $paginaAtual = 1;
        } else {
            $paginaAtual = filter_var($pagina, FILTER_SANITIZE_NUMBER_INT);
            if ($paginaAtual > ceil($totalRegistros / $registrosPorPagina)) {
                $paginaAtual = 1;
            }
        }

        //CALCULAR O OFFSET
        $offset = ($paginaAtual - 1) * $registrosPorPagina;

        //GERAR PAGINADOR
        $paginador = new Paginator($totalRegistros, $registrosPorPagina, $paginaAtual, $padraoUrl);

        //DADOS DA VIEW
        $this->viewData['tituloDaPagina'] = 'Gerir Alunos';
        $this->viewData['paginacao'] = [
            'paginaAtual' => $paginaAtual,
            'paginador' => $paginador
        ];
        $this->viewData['todosAlunos'] = $this->alunoModel->todosAlunosCurso(['curso' => $this->meuCurso, 'offset' => $offset, 'registrosPorPagina' => $registrosPorPagina]);

        $this->view('cadmin/geriralunos', $this->viewData);
    }

    //VALIDAR A FOTO
    public function validarfoto($aluid = '', $pagina = 1)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            //LIMPAR O ALUID
            $aluid = filter_var($aluid, FILTER_SANITIZE_NUMBER_INT);

            if (empty($aluid)) {
                redirect('capainel/geriralunos');
            } else if ($this->fotoModel->possoValidarFoto(['curso' => $this->meuCurso, 'aluid' => $aluid])) {
                if ($this->fotoModel->tenhoFoto($aluid) == FALSE) {
                    flash('editaraluno', 'O aluno selecionado ainda não enviou uma foto!', 'alert alert-danger');
                } else {
                    $acaofoto = filter_input(INPUT_POST, 'acaofoto', FILTER_SANITIZE_STRING);
                    $dadosAluno = $this->alunoModel->dadosAlunoId($aluid);
                    if ($acaofoto == 'ok') {
                        if ($this->fotoModel->fotoFoiValidada($aluid)) {
                            flash('editaraluno', 'Essa foto já foi aprovada!', 'alert alert-warning');
                        } else {
                            //APROVAR FOTO
                            if ($this->fotoModel->aprovarFoto($aluid)) {
                                //APAGAR PROGRESSO ANTIGO DE FOTO DO ALUNO
                                $this->alunoModel->apagarProgressoAluno($aluid, 'fotoanalise');
                                $this->alunoModel->addProgressoAluno('Foto Aprovada!', 'Realize o pagamento no CA para garantir sua carteirinha!', 'fotoaprovada', $aluid);

                                //ENVIAR UM E-MAIL INFORMANDO QUE A FOTO FOI REPROVADA
                                $texto = 'A sua foto acaba de ser aprovada no Siscard! <br /> Se ainda não realizou o pagamento da carteirinha, realize agora mesmo! <br />';
                                $texto .= '<br /> Para mais informações, consulte seu centro acadêmico!';
                                $mailer = new MailSender;
                                $mailer->setTemplateURL(PUBLIC_ROOT . DIR_STR . 'assets' . DIR_STR . 'email' . DIR_STR . 'avaliacao-foto.html');
                                $mailer->compose(array(
                                    'urlPath' => URL_ROOT . '/public/assets/email/',
                                    'textoInfo' => $texto,
                                    'nomeAluno' => $dadosAluno->nome,
                                    'fotoAcao' => 'fotoaprovada'
                                ));
                                $mailer->send(array('contato@includejr.com.br', 'Siscard'), $dadosAluno->email, 'Sua foto foi aprovada no Siscard!');

                                flash('editaraluno', 'A foto foi aprovada com sucesso!');
                            } else {
                                flash('editaraluno', 'Houve um erro ao aprovar essa foto!', 'alert alert-danger');
                            }
                        }
                    } else if ($acaofoto == 'off') {
                        //REPROVAR FOTO
                        $motivo = filter_input(INPUT_POST, 'motivo', FILTER_SANITIZE_STRING);

                        if ($this->fotoModel->reprovarFoto($aluid)) {
                            //APAGAR PROGRESSO ANTIGO DE FOTO DO ALUNO
                            $this->alunoModel->apagarProgressoAluno($aluid, 'fotoanalise');
                            $this->alunoModel->addProgressoAluno('Foto Reprovada!', 'Envie uma nova foto para ser analisada!', 'fotoanalise', $aluid);

                            //ENVIAR UM E-MAIL INFORMANDO QUE A FOTO FOI REPROVADA
                            $texto = 'Infelizmente a sua foto foi reprovada pelo seu centro acadêmico! <br /> Sugerimos que envie outra foto para ser avaliada! <br />';
                            if (!empty($motivo)) {
                                $texto .= '<br />';
                                $texto .= '<strong>Motivo:</strong> ' . htmlentities(trim($motivo));
                                $texto .= '<br />';
                            }
                            $texto .= '<br />Para mais informações, consulte seu centro acadêmico!';
                            $mailer = new MailSender;
                            $mailer->setTemplateURL(PUBLIC_ROOT . DIR_STR . 'assets' . DIR_STR . 'email' . DIR_STR . 'avaliacao-foto.html');
                            $mailer->compose(array(
                                'urlPath' => URL_ROOT . '/public/assets/email/',
                                'textoInfo' => $texto,
                                'nomeAluno' => $dadosAluno->nome,
                                'fotoAcao' => 'fotoreprovada'
                            ));
                            $mailer->send(array('contato@includejr.com.br', 'Siscard'), $dadosAluno->email, 'Sua foto foi reprovada no Siscard!');

                            flash('editaraluno', 'A foto foi reprovada com sucesso!');
                        } else {
                            flash('editaraluno', 'Houve um erro ao reprovar essa foto!', 'alert alert-danger');
                        }
                    } else {
                        flash('editaraluno', 'Por favor, selecione corretamente uma ação!', 'alert alert-danger');
                    }
                }
            }
        }
        redirect('capainel/editaraluno/' . $aluid . '/' . $pagina);
    }

    //ATUALIZAR PAGAMENTO DE ALUNO
    public function atualizarpagamento($aluid = '', $pagina = 1)
    {
        if (empty($aluid)) {
            redirect('capainel/geriralunos');
        } else if ($this->alunoModel->verificarID($aluid)) {
            if ($this->alunoModel->possoEditarAluno(['curso' => $this->meuCurso, 'aluid' => $aluid])) {
                $acaopagamento = filter_input(INPUT_POST, 'acaopagamento', FILTER_SANITIZE_NUMBER_INT);
                $acaopagamento = (is_null($acaopagamento) ? 0 : 1);
                if ($this->alunoModel->atualizarPagamento(['status' => $acaopagamento, 'aluid' => $aluid])) {
                    flash('editaraluno', 'Status de pagamento atualizado com sucesso!');
                } else {
                    flash('editaraluno', 'Houve um erro ao atualizar o status de pagamento!', 'alert alert-danger');
                }
                redirect('capainel/editaraluno/' . $aluid . '/' . $pagina);
            } else {
                flash('geriralunos', 'Você não tem permissão para editar esse aluno!', 'alert alert-danger');
                redirect('capainel/geriralunos');
            }
        } else {
            flash('geriralunos', 'O aluno informado não existe!', 'alert alert-danger');
            redirect('capainel/geriralunos');
        }
    }

    //EDITAR ALUNO
    public function editaraluno($aluid = '', $pagina = 1)
    {
        if (empty($aluid)) {
            redirect('capainel/geriralunos');
        } else if ($this->alunoModel->verificarID($aluid)) {
            if ($this->alunoModel->possoEditarAluno(['curso' => $this->meuCurso, 'aluid' => $aluid])) {

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    //INFORMAÇÕES DO ALUNO
                    $dadosAluno = $this->alunoModel->dadosAlunoId($aluid);

                    //VARIÁVEIS FORM
                    $nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
                    $nascimento = filter_input(INPUT_POST, 'nascimento', FILTER_SANITIZE_STRING);
                    $cpf = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_NUMBER_INT);
                    $rg = filter_input(INPUT_POST, 'rg', FILTER_SANITIZE_NUMBER_INT);
                    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
                    $foto = $_FILES['foto'];
                    $fotoLocation = $_FILES['foto']['tmp_name'];

                    /*
                     * VALIDAÇÃO DOS DADOS
                     */

                    //VALIDAÇÃO DE NOME
                    switch (true) {
                        case empty($nome):
                            $this->viewData['formError']['nome'] = 'Digite o nome completo do aluno!';
                            break;
                        case is_numeric(str_replace(' ', '', $nome)):
                            $this->viewData['formError']['nome'] = 'Nome inválido!';
                            break;
                        case !checkNomeCompleto($nome):
                            $this->viewData['formError']['nome'] = 'Digite o nome e sobrenome!';
                            $this->viewData['formData']['nome'] = $nome;
                            break;
                        case $nome != $dadosAluno->nome and $this->alunoModel->verificarNome($nome):
                            $this->viewData['formError']['nome'] = 'Já existe alguém cadastrado com esse nome!';
                            break;
                        default:
                            $this->viewData['formError']['nome'] = '';
                            $this->viewData['formData']['nome'] = $nome;
                            break;
                    }

                    //VALIDAÇÃO DE NASCIMENTO
                    switch (true) {
                        case empty($nascimento):
                            $this->viewData['formError']['nascimento'] = 'Digite a data de nascimento do aluno!';
                            break;
                        case preg_match('/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/', $nascimento) == 0:
                            $this->viewData['formError']['nascimento'] = 'Data de nascimento inválida!';
                            break;
                        default:
                            $this->viewData['formError']['nascimento'] = '';
                            $this->viewData['formData']['nascimento'] = $nascimento;
                    }

                    //VALIDACAO CPF
                    switch (true) {
                        case empty($cpf):
                            $this->viewData['formError']['cpf'] = 'Digite o CPF do aluno!';
                            break;
                        case !is_numeric($cpf):
                            $this->viewData['formError']['cpf'] = 'N° do CPF inválido!';
                            break;
                        case !validarCPF($cpf):
                            $this->viewData['formError']['cpf'] = 'N° do CPF inválido!';
                            break;
                        case $cpf != $dadosAluno->cpf and $this->alunoModel->verificarCPF($cpf):
                            $this->viewData['formError']['cpf'] = 'Já existe um cadastro com esse CPF!';
                            break;
                        default:
                            $this->viewData['formError']['cpf'] = '';
                            $this->viewData['formData']['cpf'] = $cpf;
                    }

                    //VALIDACAO DO RG
                    switch (true) {
                        case empty($rg):
                            $this->viewData['formError']['rg'] = 'Digite o n° do seu RG!';
                            break;
                        case !is_numeric($rg):
                            $this->viewData['formError']['rg'] = 'N° do RG inválido!';
                            break;
                        case strlen($rg) < 4:
                            $this->viewData['formError']['rg'] = 'N° do RG inválido!';
                            break;
                        case $rg != $dadosAluno->rg and $this->alunoModel->verificarRG($rg):
                            $this->viewData['formError']['rg'] = 'Já existe um cadastro com esse RG!';
                            break;
                        case $rg == $cpf:
                            $this->viewData['formError']['rg'] = 'O RG não pode ser igual ao CPF!';
                            break;
                        default:
                            $this->viewData['formError']['rg'] = '';
                            $this->viewData['formData']['rg'] = $rg;
                    }

                    //VALIDACAO DE EMAIL
                    switch (true) {
                        case empty($email):
                            $this->viewData['formError']['email'] = 'Digite seu e-mail!';
                            break;
                        case !filter_var($email, FILTER_VALIDATE_EMAIL):
                            $this->viewData['formError']['email'] = 'O e-mail informado é inválido!';
                            break;
                        case $email != $dadosAluno->email and $this->alunoModel->verificarEmail($email):
                            $this->viewData['formError']['email'] = 'Já existe um cadastro com esse e-mail!';
                            break;
                        default:
                            $this->viewData['formError']['email'] = '';
                            $this->viewData['formData']['email'] = $email;
                            break;
                    }

                    //VALIDAR A FOTO
                    switch (true) {
                        case $foto['error'] == 0 and $this->fotoModel->verificarUltimoEnvio($aluid):
                            $this->viewData['formError']['foto'] = 'Você acabou de enviar uma foto, tente novamente em alguns minutos!';
                            break;
                        case $foto['error'] == 0 and !in_array(mime_content_type($fotoLocation), array('image/jpeg', 'image/png')):
                            $this->viewData['formError']['foto'] = 'Só aceitamos fotos com as extenções: png, jpeg e jpg!';
                            break;
                        case $foto['error'] == 0 and floor(number_format($foto['size'] / 1048576, 2)) > 5:
                            $this->viewData['formError']['foto'] = 'A foto deve ter no máximo 5MB!';
                            break;
                        default:
                            $this->viewData['formError']['foto'] = '';
                            break;
                    }
                    if (!array_filter($this->viewData['formError'])) {
                        if ($this->alunoModel->atualizarCadastro(['nome' => $nome, 'nascimento' => $nascimento, 'cpf' => $cpf, 'rg' => $rg, 'curso' => $dadosAluno->cursid, 'email' => $email, 'aluid' => $aluid])) {

                            //ATUALIZAR A FOTO
                            if (!empty($fotoLocation)) {
                                try {
                                    //GERAR NOME PARA A FOTO
                                    $nomeFoto = md5(sha1(time())) . '.jpg';

                                    //CONVERTER A FOTO PARA 3X4 E SALVÁ-LA
                                    $img = Image::make($fotoLocation)->encode('jpg');
                                    $img->fit(354, 472);
                                    $img->save(UPLOAD_ROOT . DIR_STR . 'alunos' . DIR_STR . $nomeFoto);

                                    //ATUALIZAR FOTO NO BANCO
                                    $this->fotoModel->atualizarFoto(['foto' => $nomeFoto, 'aluid' => $aluid, 'status' => 1]);

                                    //ATUALIZAR PROGRESSO DO ALUNO
                                    $this->alunoModel->apagarProgressoAluno($aluid, 'fotoanalise');
                                    $this->alunoModel->addProgressoAluno('Foto Aprovada!', 'Realize o pagamento no CA para garantir sua carteirinha!', 'fotoaprovada', $aluid);

                                    //ENVIAR UM E-MAIL INFORMANDO QUE A FOTO FOI REPROVADA
                                    $texto = 'A sua foto acaba de ser aprovada no Siscard! <br /> Se ainda não realizou o pagamento da carteirinha, realize agora mesmo! <br />';
                                    $texto .= '<br /> Para mais informações, consulte seu centro acadêmico!';
                                    $mailer = new MailSender;
                                    $mailer->setTemplateURL(PUBLIC_ROOT . DIR_STR . 'assets' . DIR_STR . 'email' . DIR_STR . 'avaliacao-foto.html');
                                    $mailer->compose(array(
                                        'urlPath' => URL_ROOT . '/public/assets/email/',
                                        'textoInfo' => $texto,
                                        'nomeAluno' => $dadosAluno->nome,
                                        'fotoAcao' => 'fotoaprovada'
                                    ));
                                    $mailer->send(array('contato@includejr.com.br', 'Siscard'), $dadosAluno->email, 'Sua foto foi aprovada no Siscard!');

                                    flash('editaraluno', 'Cadastro atualizado com sucesso!');
                                } catch (NotReadableException $e) {
                                    flash('editaraluno', 'O cadastro foi atualizado, porém a foto é inválida! Tente novamente!', 'alert alert-warning');
                                }
                            } else {
                                flash('editaraluno', 'Cadastro atualizado com sucesso!');
                            }
                        } else {
                            flash('editaraluno', 'Houve um erro na atualização cadastral desse aluno!', 'alert alert-danger');
                        }
                    }
                }

                $this->viewData['dadosAluno'] = $this->alunoModel->dadosAlunoId($aluid);
                $this->viewData['dadosFoto'] = $this->fotoModel->todosDadosFoto($aluid);
                $this->viewData['tenhoCarteira'] = $this->carteiraModel->tenhoCarteira($aluid);
                if ($this->viewData['tenhoCarteira']) {
                    $this->viewData['minhaCarteira'] = $this->carteiraModel->pegarCarteiraAluno($aluid);
                }
                $this->viewData['tituloDaPagina'] = 'Editar Aluno';
                $this->viewData['pagina'] = $pagina;

                $this->view('cadmin/editaraluno', $this->viewData);
            } else {
                flash('geriralunos', 'Você não tem permissão para editar esse aluno!', 'alert alert-danger');
                redirect('capainel/geriralunos');
            }
        } else {
            flash('geriralunos', 'O aluno informado não existe!', 'alert alert-danger');
            redirect('capainel/geriralunos');
        }
    }

    //GERAR NOVO LOTE
    public function novolote()
    {
        if ($this->alunoModel->totalAlunosProntos($this->meuCurso) <= 0) {
            flash('carteirinhas', 'Não é possível gerar um lote neste momento!', 'alert alert-danger');
        } else {
            $totalAlunosProntos = $this->alunoModel->totalAlunosProntos($this->meuCurso);
            $loteId = $this->loteModel->addLote(['cursid' => $this->meuCurso, 'total' => $totalAlunosProntos]);
            if ($loteId != FALSE) {
                $idAlunos = $this->carteiraModel->gerarFilaAlunos($this->meuCurso);

                $listaIds = [];
                foreach ($idAlunos as $aluno) {
                    $listaIds[] = '(' . $loteId . ', ' . $aluno->id . ', ' . time() . ')';
                }
                $listaIdsFormatada = implode(', ', $listaIds);
                $executar = $this->carteiraModel->salvarCarteirinha($listaIdsFormatada);

                if ($executar) {
                    flash('carteirinhas', 'Lote de carteirinhas gerado com sucesso!');
                } else {
                    flash('carteirinhas', 'Houve um erro ao gerar seu lote de carteirinhas', 'alert alert-danger');
                }
            }
        }
        redirect('capainel/carteirinhas');
    }

    //CONTROLER DAS CARTEIRINHAS
    public function carteirinhas()
    {
        $totalAlunosProntos = $this->alunoModel->totalAlunosProntos($this->meuCurso);
        $this->viewData['dadosCarteirinhas'] = [
            'totalAlunosProntos' => $totalAlunosProntos,
            'meusLotes' => $this->loteModel->meusLotesGerados($this->meuCurso),
            'totalNovoLote' => $totalAlunosProntos,
            'precoLoteCarteirinhas' => $this->loteModel->precoLoteCarteirinhas($totalAlunosProntos * PRECO_UNITARIO),
        ];
        $this->viewData['tituloDaPagina'] = 'Carteirinhas';
        $this->view('cadmin/carteirinhas', $this->viewData);
    }

    //CONTROLLER DO VER LOTES
    public function verlote($loteid = '')
    {
        //LIMPAR O LOTEID
        $loteid = filter_var($loteid, FILTER_SANITIZE_NUMBER_INT);

        if (!$this->loteModel->existeLote($loteid)) {
            flash('carteirinhas', 'O lote selecionado não existe!', 'alert alert-danger');
            redirect('capainel/carteirinhas');
        } else if (!$this->loteModel->possoVerLote(['loteid' => $loteid, 'cursid' => $this->meuCurso])) {
            flash('carteirinhas', 'O lote selecionado não existe!', 'alert alert-danger');
            redirect('capainel/carteirinhas');
        } else {
            $this->viewData['tituloDaPagina'] = 'Lote N° ' . $loteid . ' - ' . $this->cursoModel->infoCurso($this->meuCurso)->nome;
            $this->viewData['infoLote'] = $this->loteModel->infoLote($loteid);
            $this->viewData['carteirasLote'] = $this->carteiraModel->pegarCarteirasLote($loteid);
            $this->viewData['infoMeuCurso'] = $this->caModel->infoMeuCurso($this->caAdminEmail);
            $this->viewData['infoDiretor'] = $this->cursoModel->infoDiretor();
            $this->viewData['valorLote'] = $this->loteModel->precoLoteCarteirinhas($this->loteModel->infoLote($loteid)->total_carteirinhas * PRECO_UNITARIO);
            $this->view('cadmin/verlote', $this->viewData);
        }
    }

    //SAIR
    public function sair()
    {
        $this->sessaoModel->apagarSessao('cadmin_email');
        redirect('cadmin');
    }
}
