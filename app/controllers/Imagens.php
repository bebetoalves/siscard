<?php

class Imagens extends Controller
{

    public function __construct()
    {
        $this->imagemModel = $this->model('Imagem');
    }

    public function index()
    {

    }

    public function fotosalunos($imagem = '', $widen = 100)
    {

        //LIMPAR AS ENTRADAS DE DADOS
        $imagem = filter_var($imagem, FILTER_SANITIZE_STRING);
        $widen = filter_var($widen, FILTER_SANITIZE_NUMBER_INT);

        if (!empty($imagem) AND $imagem != 'avatar') {
            $foto = UPLOAD_ROOT . DIR_STR . 'alunos' . DIR_STR . $imagem . '.jpg';
        } else if ($imagem == 'avatar') {
            $foto = PUBLIC_ROOT . DIR_STR . 'assets' . DIR_STR . 'images' . DIR_STR . 'faces' . DIR_STR . 'avatar.png';
        } else {
            $foto = '';
        }

        //LIMPAR O BUFFER
        ob_end_clean();

        print $this->imagemModel->carregarImagem($foto, $widen, 'png');
    }
}
