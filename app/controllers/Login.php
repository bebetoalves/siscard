<?php

class Login extends Controller
{

    public $viewData = array();

    public function __construct()
    {
        //CARREGAR OS MODELS
        $this->alunoModel = $this->model('Aluno');
        $this->cursoModel = $this->model('Curso');
        $this->sessaoModel = $this->model('Sessao');
        $this->fotoModel = $this->model('Foto');

        //PRÉ-CARREGAR VALORES QUE SERÃO PASSADOS EM TODAS AS VIEWS
        $this->viewData = [
            'tituloDaPagina' => '',
            'formData' => [],
            'formError' => [],
        ];
    }

    public function index()
    {

        //DEFINIR TITULO DA PÁGINA
        $this->viewData['tituloDaPagina'] = 'Área de Login';

        if (filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING) == 'POST') {

            //VARIAVEIS POST
            $passe = filter_input(INPUT_POST, 'passe', FILTER_SANITIZE_STRING);
            $senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING);
            $lembrarme = filter_input(INPUT_POST, 'lembrarme');

            //VARIAVEIS GERAIS
            $id = $this->alunoModel->pegarID($passe);

            //VALIDAR O PASSE
            switch (true) {
                case empty($passe):
                    $this->viewData['formError']['passe'] = 'Digite seu passe!';
                    break;
                case is_null($id):
                    $this->viewData['formError']['passe'] = 'O passe informado é inválido!';
                    break;
                default:
                    $this->viewData['formError']['passe'] = '';
                    $this->viewData['formData']['passe'] = $passe;
                    break;
            }

            //VALIDAR A SENHA
            switch (true) {
                case empty($senha):
                    $this->viewData['formError']['senha'] = 'Digite sua senha!';
                    break;
                case!password_verify($senha, $this->alunoModel->pegarSenha($id)) AND !is_null($id):
                    $this->viewData['formError']['senha'] = 'A senha informada não confere!';
                    break;
                default:
                    $this->viewData['formError']['senha'] = '';
                    break;
            }

            //VERIFICAR SE A PESSOA MARCOU LEMBRAR-ME NO FORMULÁRIO E SALVAR O PASSE EM UMA SESSÃO
            if ($lembrarme == 'on') {
                $_SESSION['lembrarme'] = $passe;
            }

            //VERIFICAR SE NÃO HOUVE ERROS / SE NÃO HOUVER, TENTAR REALIZAR CADASTRO
            if (!array_filter($this->viewData['formError'])) {
                $this->sessaoModel->novaSessao('aluno_passe', $id);
                redirect('painel/index');
            }
        }
        $this->view('pages/login', $this->viewData);
    }

}
