<?php

use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\ImageManagerStatic as Image;

class Painel extends Controller
{

    private $viewData = array();
    private $aluid;
    private $dadosAluno;

    public function __construct()
    {

        //DEFINIR ID DO ALUNO
        $this->aluid = $_SESSION['aluno_passe'];

        //CARREGAR OS MODELS
        $this->alunoModel = $this->model('Aluno');
        $this->sessaoModel = $this->model('Sessao');
        $this->cursoModel = $this->model('Curso');
        $this->fotoModel = $this->model('Foto');
        $this->mesModel = $this->model('Mensagem');

        //DADOS DO ALUNO
        $this->dadosAluno = $this->alunoModel->dadosAlunoId($this->aluid);

        //PRÉ-CARREGAR VALORES QUE SERÃO PASSADOS EM TODAS AS VIEWS
        $this->viewData = [
            'tituloDaPagina' => '',
            'dadosAluno' => $this->dadosAluno,
            'dadosFoto' => $this->fotoModel->todosDadosFoto($this->aluid),
            'listaDeCursos' => $this->cursoModel->listarTodos(),
            'dadosMensagens' => $this->mesModel->mensagensPorCAID($this->dadosAluno->cursid),
            'formData' => [],
            'formError' => [],
        ];

        //VERIFICAR SE O ALUNO ESTÁ LOGADO, CASO CONTRÁRIO REDIRECIONÁ-LO PARA O LOGIN
        if (empty($this->aluid) OR is_null($this->aluid)) {
            flash('login', 'Faça login para continuar!', 'alert alert-danger mb-0 mt-3 mb-2');
            redirect('login');
        } else if ($this->sessaoModel->existeSessao('aluno_passe')) {
            if (!$this->alunoModel->verificarID($this->aluid)) {
                $this->sessaoModel->apagarSessao('aluno_passe');
                redirect('login');
            }
        } else {
            redirect('login');
        }
    }

    public function index()
    {
        //DEFINIR TITULO DA PÁGINA
        $this->viewData['tituloDaPagina'] = 'Painel do Aluno';
        //ENVIAR PROGRESSO DO ALUNO
        $this->viewData['progressoaluno'] = $this->alunoModel->progressoAluno($this->aluid);
        //PASSAR STATUS DO ALERTA DE BOAS VINDAS E APAGÁ-LA EM SEGUIDA
        $this->viewData['boasVindas'] = isset($_SESSION['boas_vindas']) ? $_SESSION['boas_vindas'] : '';
        $this->sessaoModel->apagarSessao('boas_vindas');

        $this->view('painel/index', $this->viewData);
    }

    public function minhasinfos()
    {

        //DEFINIR TITULO DA PÁGINA
        $this->viewData['tituloDaPagina'] = 'Painel do Aluno';

        if (filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING) == 'POST') {

            $nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
            $nascimento = filter_input(INPUT_POST, 'nascimento', FILTER_SANITIZE_STRING);
            $cpf = filter_input(INPUT_POST, 'cpf');
            $rg = filter_input(INPUT_POST, 'rg');

            //VALIDAÇÃO DE NOME
            switch (true) {
                case empty($nome):
                    $this->viewData['formError']['nome'] = 'Digite seu nome completo!';
                    break;
                case is_numeric(str_replace(' ', '', $nome)):
                    $this->viewData['formError']['nome'] = 'Nome inválido!';
                    break;
                case!checkNomeCompleto($nome):
                    $this->viewData['formError']['nome'] = 'Digite um nome e sobrenome!';
                    break;
                case $this->alunoModel->verificarNome($nome) AND $nome != $this->viewData['dadosAluno']->nome:
                    $this->viewData['formError']['nome'] = 'Já existe alguém cadastrado com esse nome!';
                    break;
                default:
                    $this->viewData['formError']['nome'] = '';
                    $this->viewData['formData']['nome'] = $nome;
                    break;
            }

            //VALIDAÇÃO DE NASCIMENTO
            switch (true) {
                case empty($nascimento):
                    $this->viewData['formError']['nascimento'] = 'Digite sua data de nascimento!';
                    break;
                case preg_match('/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/', $nascimento) == 0:
                    $this->viewData['formError']['nascimento'] = 'Data de nascimento inválida!';
                    break;
                default:
                    $this->viewData['formError']['nascimento'] = '';
                    $this->viewData['formData']['nascimento'] = $nascimento;
            }

            //VALIDACAO CPF
            switch (true) {
                case empty($cpf):
                    $this->viewData['formError']['cpf'] = 'Digite o n° do seu CPF!';
                    break;
                case!is_numeric($cpf):
                    $this->viewData['formError']['cpf'] = 'N° do CPF inválido!';
                    break;
                case!validarCPF($cpf):
                    $this->viewData['formError']['cpf'] = 'N° do CPF inválido!';
                    break;
                case $this->alunoModel->verificarCPF($cpf) AND $cpf != $this->viewData['dadosAluno']->cpf:
                    $this->viewData['formError']['cpf'] = 'Já existe alguém cadastrado com esse CPF!';
                    break;
                default:
                    $this->viewData['formError']['cpf'] = '';
                    $this->viewData['formData']['cpf'] = $cpf;
            }

            //VALIDACAO DO RG
            switch (true) {
                case empty($rg):
                    $this->viewData['formError']['rg'] = 'Digite o n° do seu RG!';
                    break;
                case!is_numeric($rg):
                    $this->viewData['formError']['rg'] = 'N° do RG inválido!';
                    break;
                case strlen($rg) < 4:
                    $this->viewData['formError']['rg'] = 'N° do RG inválido!';
                    break;
                case $this->alunoModel->verificarRG($rg) AND $rg != $this->viewData['dadosAluno']->rg:
                    $this->viewData['formError']['rg'] = 'Já existe alguém cadastrado com esse RG!';
                    break;
                default:
                    $this->viewData['formError']['rg'] = '';
                    $this->viewData['formData']['rg'] = $rg;
            }

            //VERIFICAR SE NÃO HOUVE ERROS / SE NÃO HOUVER, TENTAR ATUALIZAR O CADASTRO
            if (!array_filter($this->viewData['formError'])) {
                if ($this->alunoModel->atualizarCadastro(['nome' => $nome, 'nascimento' => $nascimento, 'cpf' => $cpf, 'rg' => $rg, 'email' => $this->dadosAluno->email, 'aluid' => $this->aluid])) {
                    flash('atualMessages', 'Cadastro atualizado com sucesso!');
                    unset($this->viewData['formData']);
                } else {
                    flash('atualMessages', 'Houve um erro na atualização de seus dados!');
                }
            }
        }
        $this->view('painel/minhasinfos', $this->viewData);
    }

    public function minhafoto()
    {

        //DEFINIR TITULO DA PÁGINA
        $this->viewData['tituloDaPagina'] = 'Painel do Aluno';
        $this->viewData['statusFoto'] = $this->fotoModel->statusFormatado($this->aluid);

        if (filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING) == 'POST') {

            $foto = $_FILES['foto'];
            $fotoLocation = $_FILES['foto']['tmp_name'];

            //VALIDAR A FOTO
            switch (true) {
                case empty($fotoLocation):
                    $this->viewData['formError'] = 'Selecione uma foto!';
                    break;
                case $this->fotoModel->verificarUltimoEnvio($this->aluid):
                    $this->viewData['formError'] = 'Você acabou de enviar uma foto, tente novamente em alguns minutos!';
                    break;
                case!in_array(mime_content_type($fotoLocation), array('image/jpeg', 'image/png')):
                    $this->viewData['formError'] = 'Só aceitamos fotos com as extenções: png, jpeg e jpg!';
                    break;
                case floor(number_format($foto['size'] / 1048576, 2)) > 5:
                    $this->viewData['formError'] = 'A foto deve ter no máximo 5MB!';
                    break;
            }

            if (empty($this->viewData['formError'])) {

                try {
                    //GERAR NOME PARA A FOTO
                    $nomeFoto = md5(sha1(time())) . '.jpg';

                    //CONVERTER A FOTO PARA 3X4 E SALVÁ-LA
                    $img = Image::make($fotoLocation)->encode('jpg');
                    $img->fit(354, 472);
                    $img->save(UPLOAD_ROOT . DIR_STR . 'alunos' . DIR_STR . $nomeFoto);

                    //ATUALIZAR FOTO NO BANCO
                    $this->fotoModel->atualizarFoto(['foto' => $nomeFoto, 'aluid' => $this->aluid, 'status' => 2]);

                    //ATUALIZAR PROGRESSO DO ALUNO
                    $this->alunoModel->apagarProgressoAluno($this->aluid, "fotoanalise");
                    $this->alunoModel->addProgressoAluno("Foto em análise!", "Você enviou sua foto e ela está sendo analisada pelo CA!", "fotoanalise", $this->aluid);

                    flash('minhafoto', 'Foto enviada com sucesso!');
                } catch (NotReadableException $e) {
                    flash('minhafoto', 'Foto inválida, por favor tente novamente!', 'alert alert-danger');
                }
            }
        }
        $this->viewData['dadosFoto'] = $this->fotoModel->todosDadosFoto($this->aluid);
        $this->viewData['statusFoto'] = $this->fotoModel->statusFormatado($this->aluid);
        $this->view('painel/minhafoto', $this->viewData);
    }

    public function minhasconfigs()
    {
        //DEFINIR TITULO DA PÁGINA
        $this->viewData['tituloDaPagina'] = 'Configurações';

        if (filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING) == 'POST') {
            //VARIAVEIS POST
            $operacao = filter_input(INPUT_POST, 'operacao', FILTER_SANITIZE_NUMBER_INT);

            //ATUALIZAR EMAIL
            if ($operacao == 1) {
                //VARIAVEIS POST
                $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);

                //VALIDACAO DE EMAIL
                switch (true) {
                    case empty($email):
                        $this->viewData['formError']['email'] = 'Digite seu e-mail!';
                        break;
                    case!filter_var($email, FILTER_VALIDATE_EMAIL):
                        $this->viewData['formError']['email'] = 'O e-mail informado é inválido!';
                        break;
                    case $this->alunoModel->verificarEmail($email) AND $email != $this->viewData['dadosAluno']->email:
                        $this->viewData['formError']['email'] = 'Já existe um cadastro com esse e-mail!';
                        break;
                    default:
                        $this->viewData['formError']['email'] = '';
                        break;
                }

                //VERIFICAR SE NÃO HOUVE ERROS / SE NÃO HOUVER, TENTAR ATUALIZAR E-MAIL
                if (!array_filter($this->viewData['formError'])) {
                    if ($this->alunoModel->atualizarEmail($this->aluid, $email)) {
                        flash('minhasconfigs', 'E-mail atualizado com sucesso!');
                    } else {
                        flash('minhasconfigs', 'Houve um erro na atualização do e-mail!', 'alert alert-danger');
                    }
                }
            } //ATUALIZAR SENHA
            else if ($operacao == 2) {

                $senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING);
                $csenha = filter_input(INPUT_POST, 'csenha', FILTER_SANITIZE_STRING);

                //VALIDAÇÃO DE SENHA
                switch (true) {
                    case empty($senha):
                        $this->viewData['formError']['senha'] = 'Digite sua senha!';
                        break;
                    case strlen($senha) < 8:
                        $this->viewData['formError']['senha'] = 'A senha deve ter mais de 8 caracteres!';
                        break;
                    default:
                        $this->viewData['formError']['senha'] = '';
                        break;
                }

                //VALIDAÇÃO DE CONFIRMAÇÃO DE SENHA
                switch (true) {
                    case empty($csenha):
                        $this->viewData['formError']['csenha'] = 'Confirme sua senha!';
                        break;
                    case $senha != $csenha:
                        $this->viewData['formError']['csenha'] = 'A senha informada não confere!';
                        break;
                    default:
                        $this->viewData['formError']['csenha'] = '';
                        break;
                }

                //VERIFICAR SE NÃO HOUVE ERROS / SE NÃO HOUVER, TENTAR ATUALIZAR E-MAIL
                if (!array_filter($this->viewData['formError'])) {
                    if ($this->alunoModel->atualizarSenha($this->aluid, password_hash($senha, PASSWORD_DEFAULT))) {
                        flash('minhasconfigs', 'Senha atualizada com sucesso!');
                    } else {
                        flash('minhasconfigs', 'Houve um erro na atualização da senha!', 'alert alert-danger');
                    }
                }
            }
        }

        $this->view('painel/minhasconfigs', $this->viewData);
    }

    public function mensagem($msid = '')
    {
        if ($this->mesModel->verificarExistencia($msid)) {
            $this->viewData['tituloDaPagina'] = 'Mensagens';
            $this->viewData['dadosDaMensagem'] = $this->mesModel->dadosMensagemPorID($msid);

            $this->view('painel/mensagem', $this->viewData);
        } else {
            redirect('painel/index');
        }
    }

    public function sair()
    {
        $this->sessaoModel->apagarSessao('aluno_passe');
        redirect('');
    }

}
