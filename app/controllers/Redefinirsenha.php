<?php

class Redefinirsenha extends Controller
{

    public $viewData = array();

    public function __construct()
    {
        //CARREGAR OS MODELS
        $this->alunoModel = $this->model('Aluno');
        $this->matriculaModel = $this->model('Matricula');

        //PRÉ-CARREGAR VALORES QUE SERÃO PASSADOS EM TODAS AS VIEWS
        $this->viewData = [
            'tituloDaPagina' => '',
            'formData' => [],
            'formError' => [],
        ];
    }

    public function index()
    {


        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $matricula = filter_input(INPUT_POST, 'matricula', FILTER_SANITIZE_NUMBER_INT);
            $cpf = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_NUMBER_INT);

            //VALIDAR MATRICULA
            switch (true) {
                case empty($matricula):
                    $this->viewData['formError']['matricula'] = 'Digite o número da matrícula!';
                    break;
                case strlen($matricula) != 6:
                    $this->viewData['formError']['matricula'] = 'A matrícula deve ter 6 digitos!';
                    break;
                case $this->matriculaModel->existeMatricula($matricula) == FALSE:
                    $this->viewData['formError']['matricula'] = 'N° da matrícula inválida!';
                    break;
                case $this->alunoModel->verificarMatricula($matricula) == FALSE:
                    $this->viewData['formError']['matricula'] = 'Não há cadastro com essa matrícula!';
                    break;
                default:
                    $this->viewData['formError']['matricula'] = '';
                    $this->viewData['formData']['matricula'] = $matricula;
            }

            //VALIDAR CPF
            switch (true) {
                case empty($cpf):
                    $this->viewData['formError']['cpf'] = 'Digite o n° do seu CPF!';
                    break;
                case!is_numeric($cpf):
                    $this->viewData['formError']['cpf'] = 'N° do CPF inválido!';
                    break;
                case!validarCPF($cpf):
                    $this->viewData['formError']['cpf'] = 'N° do CPF inválido!';
                    break;
                case $this->alunoModel->verificarMatricula($matricula) AND $this->alunoModel->verificarCpfMatricula(['matricula' => $matricula, 'cpf' => $cpf]) == FALSE:
                    $this->viewData['formError']['cpf'] = 'O CPF informado não confere com a matrícula!';
                    break;
                default:
                    $this->viewData['formError']['cpf'] = '';
                    $this->viewData['formData']['cpf'] = $cpf;
            }

            //VERIFICAR SE NÃO HOUVE ERROS / SE NÃO HOUVER, TENTAR REDEFINIR SENHA
            if (!array_filter($this->viewData['formError'])) {
                $aluid = $this->alunoModel->pegarID($matricula);
                if ($this->alunoModel->prazoRedefinirSenha($aluid)) {
                    flash('redefinirsenha', 'Você redefiniu sua senha recentemente, aguarde algumas horas para tentar novamente!', 'alert alert-danger mt-3');
                } else {
                    $novaSenha = substr(str_shuffle('abcdefghijlmnopqrstuvxz' . time()), 0, 8);
                    if ($this->alunoModel->atualizarSenha($aluid, password_hash($novaSenha, PASSWORD_DEFAULT))) {

                        $dadosAluno = $this->alunoModel->dadosAlunoId($aluid);

                        //ADD NO BANCO A SOLICITAÇÃO
                        $this->alunoModel->addRedefinicaoSenha($aluid);

                        //ENVIAR O E-MAIL COM A SENHA GERADA
                        $mailer = new MailSender;
                        $mailer->setTemplateURL(PUBLIC_ROOT . DIR_STR . 'assets' . DIR_STR . 'email' . DIR_STR . 'redefinir-senha.html');
                        $mailer->compose(array(
                            'urlPath' => URL_ROOT . '/public/assets/email/',
                            'nomeAluno' => $dadosAluno->nome,
                            'novaSenha' => $novaSenha,
                        ));
                        $enviar = $mailer->send(array('contato@includejr.com.br', 'Siscard'), $dadosAluno->email, 'Sua nova senha no Siscard');


                        flash('redefinirsenha', 'Enviamos um e-mail para você com sua nova senha!', 'alert alert-success mt-3');
                    } else {
                        flash('redefinirsenha', 'Houve um erro ao tentar redefinir sua senha!', 'alert alert-danger mt-3');
                    }
                }
            }
        }

        //DEFINIR TITULO DA PÁGINA
        $this->viewData['tituloDaPagina'] = 'Redefinir a Senha';
        $this->view('pages/redefinirsenha', $this->viewData);
    }

}
