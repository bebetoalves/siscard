<?php

function validarCPF($cpf) {

    //EXTRAI SOMENTE OS NÚMEROS
    $cpf = preg_replace('/[^0-9]/is', '', $cpf);

    //VERIFICA SE FOI INFORMADO OS 11 DIGITOS NECESSÁRIOS
    if (strlen($cpf) != 11) {
        return false;
    }

    //VERIFICA SE FOI INFORMADO UMA SEQUENCIA DE NÚMEROS REPETIDOS
    if (preg_match('/(\d)\1{10}/', $cpf)) {
        return false;
    }

    //FAZ O CALCULO PARA VALIDAR O CPF
    for ($t = 9; $t < 11; $t++) {
        for ($d = 0, $c = 0; $c < $t; $c++) {
            $d += $cpf{$c} * (($t + 1) - $c);
        }
        $d = ((10 * $d) % 11) % 10;
        if ($cpf{$c} != $d) {
            return false;
        }
    }
    return true;
}
