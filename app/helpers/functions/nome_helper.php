<?php

/*
 * HELPER PARA AJUDAR NO TRATAMENTO DO NOME
 */

function checkNomeCompleto($nome) {
    if (empty($nome)) {
        return FALSE;
    } else {
        $separar = explode(" ", $nome);
        if (count($separar) <= 1) {
            return FALSE;
        } else if (strlen($separar[0]) <= 1) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
}

function tratarNomeCompleto($nome) {
    $saida = "";
    $nome = mb_strtolower($nome);
    $nome = explode(" ", $nome);
    for ($i = 0; $i < count($nome); $i++) {

        if ($nome[$i] == "de" or $nome[$i] == "da" or $nome[$i] == "e" or $nome[$i] == "dos" or $nome[$i] == "do") {
            $saida .= $nome[$i] . ' ';
        } else {
            $saida .= ucfirst($nome[$i]) . ' ';
        }
    }
    return $saida;
}
