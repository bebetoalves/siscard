<?php

//FUNÇÃO PARA REDIRECIONAMENTO
function redirect($page, $time = '') {
    if (empty($time)) {
        header('Location: ' . URL_ROOT . '/' . $page);
    } else {
        header('Refresh:' . $time . '; url=' . URLROOT . '/' . $page);
    }
}
