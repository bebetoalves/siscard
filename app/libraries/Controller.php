<?php

/*
 * CLASSE CORE DO CONTROLLER
 * 
 * AQUI SERÁ CARREGADO OS MODELS E VIEWS.
 */

class Controller {

    //CARREGAR O MODEL ATRAVÉS DO CONTROLLER
    public function model($model) {
        //FAZER REQUIRE DO MODEL
        require_once '../app/models/' . $model . '.php';
        //INICIAR O MODEL
        return new $model();
    }

    //CARREGAR A VIEW A ATRAVÉS DO CONTROLLER
    public function view($view, $data = []) {
        
        $data['view'] = $view;
        
        extract($data);
        
        //VERIFICAR SE A VIEW EXISTE
        if (file_exists('../app/views/' . $view . '.php')) {
            //FAZER REQUIRE DA VIEW
            require_once '../app/views/' . $view . '.php';
        } else {
            //RETORNAR ERRO CASO NÃO EXISTA
            $erro = 'A view <strong>' . $view . '</strong> não existe!';
            die($erro);
        }
    }

}
