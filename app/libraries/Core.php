<?php

/*
 * Classes Core do App
 * Cria as URL e carrega o core controller. 
 * FORMATO DA URL: /controller/method/params
 */

class Core {

    protected $currentController = 'Login';
    protected $currentMethod = 'index';
    protected $params = [];

    public function __construct() {
        $url = $this->getUrl();

        // Ver se há controllers no primeiro valor
        if (file_exists('../app/controllers/' . ucwords($url[0]) . '.php')) {
            // Se existir, definir como controller
            $this->currentController = ucwords($url[0]);
            // Remover o índice 0
            unset($url[0]);
        }

        // Require o controle
        require_once '../app/controllers/' . $this->currentController . '.php';

        // Iniciar a classe do controller
        $this->currentController = new $this->currentController;

        // Verificar a segunda parte da URL
        if (isset($url[1])) {
            // Verificar se existe o metodo requisitado
            if (method_exists($this->currentController, $url[1])) {
                $this->currentMethod = $url[1];
                unset($url[1]);
            }
        }

        // Pegar os params
        $this->params = $url ? array_values($url) : [];

        // Callback com os arrays do params
        call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
    }

    public function getUrl() {
        if (isset($_GET['url'])) {
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);
            return $url;
        }
    }

}
