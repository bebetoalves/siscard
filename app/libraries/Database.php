<?php

/*
 * CLASSE CORE PARA CONEXÃO AO BANCO DE DADOS
 */

class Database {

    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;
    private $dbh;
    private $error;
    private $stmt;

    public function __construct() {
        //CONFIGURAR O DNS
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        );

        //INICIAR O PDO
        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        } catch (PDOException $e) {
            //MOSTRAR ERRO EM CASO DE FALHA DE CONEXÃO
            die($this->error = $e->getMessage());
        }
    }

    //QUERY COM PREPARED STATEMENTS
    public function query($query) {
        $this->stmt = $this->dbh->prepare($query);
    }

    //BIND VALUES COM FORMATAÇÃO AUTOMÁTICA DE DADOS
    public function bind($param, $value, $type = null) {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    //EXECUTAR A QUERY
    public function execute() {
        return $this->stmt->execute();
    }

    //RETORNAR ARRAY DE OBJETOS
    public function resultSet() {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

    //RETORNAR UM ÚNICO RESULTADO
    public function single() {
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }

    //RETORNAR TOTAL DE LINHAS
    public function rowCount() {
        return $this->stmt->rowCount();
    }

    //RETORNAR O ÚLTIMO INSERIDO
    public function lastInsertId() {
        return $this->dbh->lastInsertId();
    }

    //RETORNAR POSSIVEIS ERROS
    public function errorInfo() {
        return $this->stmt->errorInfo();
    }

}
