<?php

class Aluno
{

    //VARIAVEIS
    private $db;

    //CONECTAR AO BANCO DE DADOS
    public function __construct()
    {
        $this->db = new Database();
    }

    public function testet()
    {
        $this->db->query('SELECT alu.*, fot.* FROM ca_alunos AS alu JOIN ca_fotos AS fot ON alu.id = fot.aluid WHERE alu.cursid=:cursid AND fot.foto IS NOT NULL');
        $this->db->bind(':cursid', 5);
        $dadosAlunos = $this->db->resultSet();

        $this->db->query('SELECT * FROM ca_cursos WHERE id=:cursid');
        $this->db->bind(':cursid', 5);
        $dadosCurso = $this->db->resultSet();
        return array(
            'alunos' => $dadosAlunos,
            'curso' => array_shift($dadosCurso)
        );
    }

    //APAGAR PROGRESSO
    public function apagarProgressoAluno($id, $tag)
    {
        $this->db->query('DELETE FROM ca_progressos_alunos WHERE aluid=:id AND tag=:tag');
        $this->db->bind(':id', $id);
        $this->db->bind(':tag', $tag);
        return $this->db->execute();
    }

    //ADICIONAR PROGRESSO AO ALUNO
    public function addProgressoAluno($titulo, $descricao, $tag, $aluid)
    {
        $this->db->query("INSERT INTO ca_progressos_alunos(titulo, descricao, tag, dataregistro, aluid) VALUES (:titulo, :descricao, :tag, :dataregistro, :aluid)");
        $this->db->bind(":titulo", $titulo);
        $this->db->bind(":descricao", $descricao);
        $this->db->bind(":tag", $tag);
        $this->db->bind(":dataregistro", time());
        $this->db->bind(":aluid", $aluid);

        return $this->db->execute();
    }

    //FUNÇÃO PROGRESSO ALUNO
    public function progressoAluno($id)
    {
        $this->db->query("SELECT * FROM ca_progressos_alunos WHERE aluid = :id ORDER BY id ASC");
        $this->db->bind(":id", $id);

        return $this->db->resultSet();
    }

    //TRANSFORMAR MATRÍCULA OU E-MAIL EM ID
    public function pegarID($documento)
    {
        if (is_numeric($documento)) {
            $this->db->query('SELECT * FROM ca_alunos WHERE matricula=:documento');
        } else {
            $this->db->query('SELECT * FROM ca_alunos WHERE email=:documento');
        }
        $this->db->bind(':documento', $documento);
        $single = $this->db->single();
        if (empty($single)) {
            return NULL;
        } else {
            return $single->id;
        }
    }

    //RETORNAR SENHA NOME DO ALUNO
    public function pegarNome($id)
    {
        $this->db->query('SELECT * FROM ca_alunos WHERE id=:id');
        $this->db->bind(':id', $id);

        $single = $this->db->single();
        if (empty($single)) {
            return NULL;
        } else {
            return $single->nome;
        }
    }

    //RETORNAR SENHA DO ALUNO
    public function pegarSenha($id)
    {
        $this->db->query('SELECT * FROM ca_alunos WHERE id=:id');
        $this->db->bind(':id', $id);

        $single = $this->db->single();
        if (empty($single)) {
            return NULL;
        } else {
            return $single->senha;
        }
    }

    //VERIFICAR SE UM ID EXISTE
    public function verificarID($id)
    {
        $this->db->query('SELECT * FROM ca_alunos WHERE id=:id');
        $this->db->bind(':id', $id);
        $this->db->execute();
        $total = $this->db->rowCount();

        if ($total > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //VERIFICAR SE EXISTE ALGUM ALUNO CADASTRADO COM O MESMO NOME
    public function verificarNome($nome)
    {
        $this->db->query('SELECT * FROM ca_alunos WHERE nome=:nome');
        $this->db->bind(':nome', $nome);
        $this->db->execute();

        $total = $this->db->rowCount();
        if ($total > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //VERIFICAR SE EXISTE ALGUM ALUNO CADASTRADO COM O MESMO CPF
    public function verificarCPF($cpf)
    {
        $this->db->query('SELECT * FROM ca_alunos WHERE cpf=:cpf');
        $this->db->bind(':cpf', $cpf);
        $this->db->execute();

        $total = $this->db->rowCount();
        if ($total > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //VERIFICAR SE O CPF INFORMADO BATE COM A MATRÍCULA DO ALUNO
    public function verificarCpfMatricula($dados)
    {
        $this->db->query('SELECT * FROM ca_alunos WHERE matricula=:matricula AND cpf=:cpf');
        $this->db->bind(':matricula', $dados['matricula']);
        $this->db->bind(':cpf', $dados['cpf']);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //VERIFICAR SE EXISTE ALGUM ALUNO CADASTRADO COM O MESMO CPF
    public function verificarRG($rg)
    {
        $this->db->query('SELECT * FROM ca_alunos WHERE rg=:rg');
        $this->db->bind(':rg', $rg);
        $this->db->execute();

        $total = $this->db->rowCount();
        if ($total > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //VERIFICAR SE EXISTE ALGUM ALUNO CADASTRADO COM O MESMO EMAIL
    public function verificarEmail($email)
    {
        $this->db->query('SELECT * FROM ca_alunos WHERE email=:email');
        $this->db->bind(':email', $email);
        $this->db->execute();

        $total = $this->db->rowCount();
        if ($total > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //RETORNAR TODOS OS DADOS DE UM ALUNO PELO ID
    public function dadosAlunoId($id)
    {
        $this->db->query('SELECT al.id, al.nome, al.matricula, al.nascimento, al.cpf, al.rg, al.cursid, c.nome as nome_curso, c.coordenador as coordenador_curso, c.ass_coordenador, al.email, al.senha, al.status, al.status_pagamento, al.dataregistro FROM ca_alunos AS al JOIN ca_cursos as c ON al.cursid = c.id WHERE al.id=:id');
        $this->db->bind(':id', $id);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            $dados = $this->db->resultSet();
            return array_shift($dados);
        } else {
            return FALSE;
        }
    }

    //RETORNAR O TOTAL DE ALUNOS DE UM CURSO
    public function totalAlunosCurso($curso)
    {
        $this->db->query('SELECT l.* FROM ca_alunos AS l JOIN ca_fotos AS f ON f.aluid = l.id WHERE l.cursid=:cursid ORDER BY f.status DESC');
        $this->db->bind(':cursid', $curso);
        $this->db->execute();
        return $this->db->rowCount();
    }

    //RETORNAR OS DADOS DE TODOS OS ALUNOS DE UM CURSO
    public function todosAlunosCurso($dados = [])
    {
        $this->db->query('SELECT l.id AS aluid, l.matricula, l.nome, l.status_pagamento, f.foto, f.status AS foto_status, COUNT(ca_carteiras.id) AS total_carteirinhas FROM ca_alunos l LEFT OUTER JOIN ca_fotos f ON f.aluid = l.id LEFT OUTER JOIN ca_carteiras ON ca_carteiras.aluid = l.id WHERE l.cursid = :cursid GROUP BY l.id, l.matricula, l.nome, l.status_pagamento, f.foto, f.status ORDER BY l.nome LIMIT :offset, :registrosPorPagina');
        $this->db->bind(':cursid', $dados['curso']);
        $this->db->bind(':offset', $dados['offset']);
        $this->db->bind(':registrosPorPagina', $dados['registrosPorPagina']);
        return $this->db->resultSet();
    }

    //VERIFICAR SE EXISTE ALGUM ALUNO CADASTRADO COM A MESMA MATRICULA
    public function verificarMatricula($matricula)
    {
        $this->db->query('SELECT * FROM ca_alunos WHERE matricula=:matricula');
        $this->db->bind(':matricula', $matricula);
        $this->db->execute();

        $total = $this->db->rowCount();
        if ($total > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //CADASTRO DE ALUNOS
    public function cadastrarAluno($dados)
    {
        $this->db->query('INSERT INTO ca_alunos (nome, matricula, nascimento, cpf, rg, cursid, senha, email, dataregistro) VALUES (:nome, :matricula, :nascimento, :cpf, :rg, :cursid, :senha, :email, :dataregistro)');
        $this->db->bind(':nome', $dados['nome']);
        $this->db->bind(':matricula', $dados['matricula']);
        $this->db->bind(':nascimento', $dados['nascimento']);
        $this->db->bind(':cpf', $dados['cpf']);
        $this->db->bind(':rg', $dados['rg']);
        $this->db->bind(':cursid', $dados['cursid']);
        $this->db->bind(':senha', password_hash($dados['senha'], PASSWORD_DEFAULT));
        $this->db->bind(':email', $dados['email']);
        $this->db->bind(':dataregistro', time());

        if ($this->db->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }

    //APAGAR CADASTRO DE ALUNO PELO ID
    public function apagarCadastro($id)
    {
        $this->db->query('DELETE FROM ca_alunos WHERE id=:id');
        $this->db->bind('id', $id);
        return $this->db->execute();
    }

    //ATUALIZAR DADOS
    public function atualizarCadastro($dados)
    {

        $this->db->query('UPDATE ca_alunos SET nome=:nome, nascimento=:nascimento, cpf=:cpf, rg=:rg, email=:email WHERE id=:aluid');
        $this->db->bind(':nome', mb_strtoupper($dados['nome']));
        $this->db->bind(':nascimento', $dados['nascimento']);
        $this->db->bind(':cpf', $dados['cpf']);
        $this->db->bind(':rg', $dados['rg']);
        $this->db->bind(':email', $dados['email']);
        $this->db->bind(':aluid', $dados['aluid']);

        return $this->db->execute();
    }

    //ATUALIZAR E-MAIL
    public function atualizarEmail($id, $email)
    {
        $this->db->query('UPDATE ca_alunos SET email=:email WHERE id=:id');
        $this->db->bind(':email', $email);
        $this->db->bind(':id', $id);
        return $this->db->execute();
    }

    //ATUALIZAR SENHA
    public function atualizarSenha($id, $senha)
    {
        $this->db->query('UPDATE ca_alunos SET senha=:senha WHERE id=:id');
        $this->db->bind(':senha', $senha);
        $this->db->bind(':id', $id);
        return $this->db->execute();
    }

    //TOTAL ALUNOS CADASTRADOS
    public function totalAlunosCadastrados($id)
    {
        $this->db->query("SELECT * FROM ca_alunos WHERE cursid=:id");
        $this->db->bind(":id", $id);
        $this->db->execute();

        return $this->db->rowCount();
    }

    //RETORNAR O TOTAL DE ALUNOS CADASTRADOS POR MES
    public function alunosPorMes($curso)
    {
        $chartData = array();
        for ($x = 1; $x <= 12; $x++) {
            $chartData[$x] = $this->totalAlunosMes($x, $curso);
        }
        return $chartData;
    }

    //TOTAL DE ALUNOS POR MES
    public function totalAlunosMes($mes, $cursid)
    {
        $this->db->query("SELECT * FROM ca_alunos WHERE MONTH(FROM_UNIXTIME(dataregistro)) = :mes AND YEAR(FROM_UNIXTIME(dataregistro)) = :ano AND cursid=:cursid");
        $this->db->bind(":mes", $mes);
        $this->db->bind(":ano", date("Y"));
        $this->db->bind(":cursid", $cursid);
        $this->db->execute();

        return $this->db->rowCount();
    }

    //TOTAL DE ALUNOS QUE PAGARAM DE UM CURSO
    public function totalAlunosQuePagaram($cursid)
    {
        $this->db->query('SELECT * FROM ca_alunos WHERE status_pagamento=:status AND cursid=:cursid');
        $this->db->bind(':status', 1);
        $this->db->bind(':cursid', $cursid);
        $this->db->execute();
        return $this->db->rowCount();
    }

    //TOTAL ALUNOS PRONTOS PARA GERAR CARTEIRINHAS
    public function totalAlunosProntos($cursid)
    {
        $this->db->query('SELECT alu.* FROM ca_alunos AS alu JOIN ca_fotos AS fot ON fot.aluid = alu.id WHERE fot.status=:status_foto AND alu.status_pagamento=:status_pagamento AND alu.cursid=:cursid AND alu.gerado=:status_gerado');
        $this->db->bind(':status_pagamento', 1);
        $this->db->bind(':status_foto', 1);
        $this->db->bind(':cursid', $cursid);
        $this->db->bind(':status_gerado', 0);
        $this->db->execute();
        return $this->db->rowCount();
    }

    //RETORNAR O STATUS DE PAGAMENTO DE UM ALUNO
    public function statusPagamento($aluid)
    {
        $this->db->query('SELECT status_pagamento FROM ca_alunos WHERE id=:aluid');
        $this->db->bind(':aluid', $aluid);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            $status = $this->db->single()->status_pagamento;
            return $status;
        } else {
            return FALSE;
        }
    }

    //ATUALIZAR SITUAÇÃO DE PAGAMENTO DO ALUNO
    public function atualizarPagamento($dados)
    {
        $this->db->query('UPDATE ca_alunos SET status_pagamento=:status WHERE id=:aluid');
        $this->db->bind(':status', $dados['status']);
        $this->db->bind(':aluid', $dados['aluid']);
        return $this->db->execute();
    }

    //ATUALIZAR SITUAÇÃO DE PAGAMENTO DE MÚLTIPLOS ALUNOS
    public function atualizarPagamentoAlunos($dados)
    {
        $this->db->query('UPDATE ca_alunos SET status_pagamento=:status WHERE id=:id');
        foreach ($dados['alunos'] as $aluno) {
            $this->db->bind(':status', $dados['status']);
            $this->db->bind(':id', $aluno);
            $this->db->execute();
        }
    }

    //VERIFICAR SE UM CURSO PODE EDITAR UM ALUNO
    public function possoEditarAluno($dados)
    {
        $this->db->query('SELECT * FROM ca_alunos WHERE id=:aluid AND cursid=:cursid');
        $this->db->bind(':aluid', $dados['aluid']);
        $this->db->bind(':cursid', $dados['curso']);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //FUNÇÃO PARA ADD SOLICITAÇÃO DE REDEFINIÇÃO DE SENHA DO ALUNO
    public function addRedefinicaoSenha($aluid)
    {
        $this->db->query('INSERT INTO ca_redefinicaosenha (aluid, ip, dataregistro) VALUES (:aluid, :ip, :dataregistro)');
        $this->db->bind(':aluid', $aluid);
        $this->db->bind(':ip', $_SERVER['REMOTE_ADDR']);
        $this->db->bind(':dataregistro', time());
        return $this->db->execute();
    }

    //VERIFICAR SE O ALUNO JÁ TENTOU REDEFINIR SENHA NAS ULTIMAS 12 HORAS
    public function prazoRedefinirSenha($aluid)
    {
        $this->db->query('SELECT dataregistro FROM ca_redefinicaosenha WHERE aluid=:aluid ORDER BY dataregistro DESC');
        $this->db->bind(':aluid', $aluid);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            $data = ($this->db->single()->dataregistro) + 43200; //12 HORAS
            if ($data > time()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

}
