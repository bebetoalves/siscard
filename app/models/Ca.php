<?php

class Ca
{

    //VARIAVEIS
    private $db;

    //CONECTAR AO BANCO DE DADOS
    public function __construct()
    {
        $this->db = new Database();
    }

    //PEGAR DADOS DO CA PELO E-MAIL
    public function meusDados($email)
    {
        $this->db->query('SELECT * FROM ca_admin WHERE email=:email');
        $this->db->bind(':email', $email);
        return $this->db->single();
    }

    //PEGAR DADOS DO CURSO DO CA
    public function infoMeuCurso($email)
    {
        $id = $this->meusDados($email)->id;
        $this->db->query('SELECT c.* FROM ca_cursos AS c JOIN ca_admin_cursos AS a ON c.id = a.cursid WHERE a.admid=:admid');
        $this->db->bind(':admid', $id);
        $this->db->execute();
        if ($this->db->rowCount() > 0) {
            $dados = $this->db->resultSet();
            return array_shift($dados);
        } else {
            return FALSE;
        }
    }

    //PEGAR O CURSO QUE O CA É RESPONSÁVEL
    public function meuCurso($email)
    {
        $this->db->query('SELECT dc.* FROM ca_admin_cursos AS dc JOIN ca_admin AS ad ON dc.admid = ad.id WHERE ad.email=:email');
        $this->db->bind(':email', $email);
        return $this->db->single()->cursid;
    }

    //VERIFICAR E-MAIL
    public function verificarEmail($email)
    {
        $this->db->query('SELECT * FROM ca_admin WHERE email=:email');
        $this->db->bind(':email', $email);
        $this->db->execute();

        if ($this->db->resultSet()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //ATUALIZAR E-MAIL
    public function atualizarEmail($id, $email)
    {
        $this->db->query('UPDATE ca_admin SET email=:email WHERE id=:id');
        $this->db->bind(':email', $email);
        $this->db->bind(':id', $id);
        return $this->db->execute();
    }

    //ATUALIZAR SENHA
    public function atualizarSenha($id, $senha)
    {
        $this->db->query('UPDATE ca_admin SET senha=:senha WHERE id=:id');
        $this->db->bind(':senha', $senha);
        $this->db->bind(':id', $id);
        return $this->db->execute();
    }

    //VERIFCAR SE O ADMIN TEM CURSO ASSOCIADO A CONTA
    public function tenhoCursoAssociado($email)
    {
        $this->db->query('SELECT adc.* FROM ca_admin_cursos AS adc JOIN ca_admin AS ad ON ad.id = adc.admid WHERE email=:email');
        $this->db->bind(':email', $email);
        $this->db->execute();
        if ($this->db->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
