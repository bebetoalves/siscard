<?php

class Carteira
{

    //VARIAVEIS
    private $db;

    //CONECTAR AO BANCO DE DADOS
    public function __construct()
    {
        $this->db = new Database();
    }

    //SALVAR CARTEIRINHA DO ALUNO NO BANCO
    public function salvarCarteirinha($dados)
    {
        $this->db->query('INSERT INTO ca_carteiras (loteid, aluid, dataregistro) VALUES ' . $dados . '');
        $executar = $this->db->execute();
        if ($executar) {
            $this->db->query('UPDATE ca_alunos SET gerado=:gerado WHERE id IN (SELECT aluid FROM ca_carteiras)');
            $this->db->bind(':gerado', 1);
            return $this->db->execute();
        }
    }

    //RETORNAR O TOTAL DE CARTEIRINHAS GERADAS POR MÊS
    public function carteirasGeradasPorMes($curso)
    {
        $chartData = array();

        for ($x = 1; $x <= 12; $x++) {
            $chartData[$x] = $this->totalCarteirinhaMes($x, $curso);
        }

        return $chartData;
    }

    //TOTAL DE CARTEIRINHAS GERADAS POR MES
    public function totalCarteirinhaMes($mes, $cursid)
    {
        $this->db->query("SELECT COALESCE(SUM(total),0) AS totalCarteirinhaMes FROM ca_lotes WHERE MONTH(FROM_UNIXTIME(dataregistro)) = :mes AND YEAR(FROM_UNIXTIME(dataregistro)) = :ano AND cursid=:cursid");
        $this->db->bind(":mes", $mes);
        $this->db->bind(":ano", date("Y"));
        $this->db->bind(":cursid", $cursid);
        return $this->db->single()->totalCarteirinhaMes;
    }

    //VERIFICAR SE O ALUNO TEM CARTEIRA
    public function tenhoCarteira($aluid)
    {
        $this->db->query('SELECT * FROM ca_carteiras WHERE aluid=:aluid');
        $this->db->bind(':aluid', $aluid);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //PEGAR CARTEIRINHA DE UM ALUNO
    public function pegarCarteiraAluno($aluid)
    {
        $this->db->query('SELECT * FROM ca_carteiras WHERE aluid=:aluid ORDER BY id DESC LIMIT 1');
        $this->db->bind(':aluid', $aluid);
        $dados = $this->db->resultSet();
        return array_shift($dados);
    }

    //RETORNAR O TOTAL DE CARTEIRAS GERADAS
    public function totalCarteirasGeradas($curso)
    {
        $this->db->query('SELECT COALESCE(SUM(total),0) AS totalCarteirasGeradas FROM ca_lotes WHERE cursid=:cursid');
        $this->db->bind(':cursid', $curso);
        $dados = $this->db->single();
        return $dados->totalCarteirasGeradas;
    }

    //RETORNAR O TOTAL DE CARTEIRINHAS JÁ GERADAS PARA UM LOTE X
    public function totalCarteirasGeradasLote($loteid)
    {
        $this->db->query('SELECT * FROM ca_carteiras WHERE loteid=:loteid');
        $this->db->bind(':loteid', $loteid);
        $this->db->execute();
        return $this->db->rowCount();
    }

    //GERAR FILA DE ALUNOS
    public function gerarFilaAlunos($cursid)
    {
        $this->db->query('SELECT alu.id FROM ca_alunos AS alu JOIN ca_fotos AS fot ON fot.aluid = alu.id WHERE fot.status=:status_foto AND alu.status_pagamento=:status_pagamento AND alu.cursid=:cursid AND alu.gerado=:gerado');
        $this->db->bind(':status_pagamento', 1);
        $this->db->bind(':status_foto', 1);
        $this->db->bind(':gerado', 0);
        $this->db->bind(':cursid', $cursid);
        return $this->db->resultSet();
    }

    //RETORNAR TOTAL DE CARTEIRINHAS DE UM LOTE
    public function totalCarteirasLote($loteid)
    {
        $this->db->query('SELECT * FROM ca_lotes WHERE id=:loteid');
        $this->db->bind(':loteid', $loteid);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            return $this->db->single()->total;
        } else {
            return FALSE;
        }
    }

    //PEGAR TODAS AS CARTEIRINHAS DE UM LOTE
    public function pegarCarteirasLote($loteid)
    {
        $this->db->query('SELECT aluno.cursid, aluno.nome AS nome_aluno, aluno.nascimento, aluno.cpf, aluno.rg, aluno.matricula, ca_fotos.foto FROM ca_carteiras cart INNER JOIN ca_alunos aluno ON cart.aluid = aluno.id INNER JOIN ca_fotos ON ca_fotos.aluid = aluno.id WHERE cart.loteid = :loteid ORDER BY nome_aluno');
        $this->db->bind(':loteid', $loteid);
        return $this->db->resultSet();
    }
}
