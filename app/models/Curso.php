<?php

class Curso
{

    // VARIAVEIS
    private $db;

    // CONECTAR AO BANCO DE DADOS
    public function __construct()
    {
        $this->db = new Database();
    }

    //LISTAR TODOS OS CURSOS DISPONÍVEIS

    public function listarTodos()
    {
        $this->db->query('SELECT * FROM ca_cursos');
        return $this->db->resultSet();
    }

    //VERIFICAR SE EXISTE CURSO PELO ID
    public function existeCurso($id)
    {
        $this->db->query('SELECT * FROM ca_cursos WHERE id=:id');
        $this->db->bind(':id', $id);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //RETORNAR TODAS AS INFORMAÇÕES DE UM CURSO
    public function infoCurso($id)
    {
        $this->db->query('SELECT * FROM ca_cursos WHERE id=:id');
        $this->db->bind(':id', $id);
        return $this->db->single();
    }

    //FUNÇÃO PARA RETORNAR O STATUS DO CADASTRO
    public function statusCadastro($id)
    {
        $this->db->query('SELECT * FROM ca_cursos WHERE id=:id');
        $this->db->bind(':id', $id);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            return $this->db->single()->cadastros;
        } else {
            return NULL;
        }
    }

    //FUNÇÃO PARA MUDAR O STATUS DO CADASTRO
    //1 = ABERTO
    //0 = FECHADO
    public function atualizarCadastro($dados)
    {
        $this->db->query('UPDATE ca_cursos SET cadastros=:cadastros WHERE id=:id');
        $this->db->bind(':cadastros', $dados['status']);
        $this->db->bind(':id', $dados['id']);
        return $this->db->execute();
    }

    //INFORMAÇÕES DO DIRETOR
    public function infoDiretor()
    {
        $this->db->query('SELECT * FROM ca_diretor ORDER BY id DESC LIMIT 1');
        $this->db->execute();
        $dados = $this->db->resultSet();
        return array_shift($dados);
    }

}
