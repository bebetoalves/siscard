<?php

class Foto
{

    //VARIAVEIS
    private $db;

    //CONECTAR AO BANCO DE DADOS
    public function __construct()
    {
        $this->db = new Database();
    }

    //FUNÇÃO PARA VERIFICAR SE HÁ FLOOD NO ENVIO DE FOTO
    public function verificarUltimoEnvio($aluid)
    {
        $this->db->query('SELECT data FROM ca_fotos WHERE foto IS NOT NULL AND aluid=:aluid');
        $this->db->bind(':aluid', $aluid);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            $data = ($this->db->single()->data) + 60; //1 MINUTO

            if ($data > time()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {

        }
    }

    //FUNÇÃO PARA VERIFICAR SE O ALUNO TEM FOTO
    public function tenhoFoto($aluid)
    {
        $this->db->query('SELECT * FROM ca_fotos WHERE aluid=:aluid');
        $this->db->bind(':aluid', $aluid);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            $foto = $this->db->single()->foto;
            if (!is_null($foto)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    //ADD UMA FOTO EM BRANCO PARA O USUÁRIO
    public function addFoto($aluid)
    {
        $this->db->query('INSERT INTO ca_fotos (aluid, data) VALUES (:aluid, :data)');
        $this->db->bind(':aluid', $aluid);
        $this->db->bind(':data', time());
        return $this->db->execute();
    }

    //ATUALIZAR FOTO
    public function atualizarFoto($dados)
    {

        //APAGAR FOTO ANTIGA
        if ($this->tenhoFoto($dados['aluid'])) {
            $foto = $this->todosDadosFoto($dados['aluid'])->foto;
            @unlink(UPLOAD_ROOT . DIR_STR . 'alunos' . DIR_STR . $foto);
        }

        $this->db->query('UPDATE ca_fotos SET foto=:foto, status=:status, data=:data WHERE aluid=:aluid');
        $this->db->bind(':foto', $dados['foto']);
        $this->db->bind(':status', $dados['status']);
        $this->db->bind(':data', time());
        $this->db->bind(':aluid', $dados['aluid']);
        return $this->db->execute();
    }

    //RETORNAR TODOS OS DADOS DA TABELA BASEADO EM UM ALUID
    public function todosDadosFoto($aluid)
    {
        $this->db->query('SELECT * FROM ca_fotos WHERE aluid=:aluid');
        $this->db->bind(':aluid', $aluid);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            $dados = $this->db->resultSet();
            return array_shift($dados);
        } else {
            return FALSE;
        }
    }

    //RETORNAR O TOTAL DE FOTOS PARA VALIDAÇÃO
    public function totalFotosValidacao($curso)
    {
        $this->db->query('SELECT l.id AS aluid, l.matricula, l.nome, l.nascimento, l.cpf, l.rg, l.email, l.status, l.dataregistro AS dataregistro_aluno, f.id AS fotid, f.foto, f.data AS dataregistro_foto FROM ca_alunos AS l JOIN ca_fotos AS f ON f.aluid = l.id WHERE l.cursid = :cursid AND f.status = :status');
        $this->db->bind(':cursid', $curso);
        $this->db->bind(':status', 2);
        $this->db->execute();

        return $this->db->rowCount();
    }

    //VERIFICAR SE FOTO JÁ FOI VALIDADA
    public function fotoFoiValidada($aluid)
    {
        $this->db->query('SELECT * FROM ca_fotos WHERE aluid=:aluid');
        $this->db->bind(':aluid', $aluid);

        if ($this->db->single()->status == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //VERIFICAR SE UM DETERMINADO CURSO PODE VALIDAR A FOTO DE UM ALUNO
    public function possoValidarFoto($dados)
    {
        $this->db->query('SELECT f.* FROM ca_fotos AS f JOIN ca_alunos AS l ON f.aluid = l.id WHERE f.aluid=:aluid AND l.cursid=:cursid');
        $this->db->bind(':aluid', $dados['aluid']);
        $this->db->bind(':cursid', $dados['curso']);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //VALIDAR A FOTO
    public function aprovarFoto($aluid)
    {
        $this->db->query('UPDATE ca_fotos SET status=:status WHERE aluid=:aluid');
        $this->db->bind(':status', 1);
        $this->db->bind(':aluid', $aluid);
        return $this->db->execute();
    }

    //RECUSAR FOTO
    public function reprovarFoto($aluid)
    {
        //APAGAR FOTO DA PASTA
        $this->db->query('SELECT * FROM ca_fotos WHERE aluid=:aluid');
        $this->db->bind(':aluid', $aluid);
        $foto = $this->db->single()->foto;
        @unlink(UPLOAD_ROOT . DIR_STR . 'alunos' . DIR_STR . $foto);

        //APAGAR FOTO DO BANCO
        $this->db->query('UPDATE ca_fotos SET foto=:foto, status=:status WHERE aluid=:aluid');
        $this->db->bind(':aluid', $aluid);
        $this->db->bind(':foto', NULL);
        $this->db->bind(':status', 0);

        return $this->db->execute();
    }

    //FORMATAR O STATUS
    public function statusFormatado($id)
    {
        $dados = $this->todosDadosFoto($id);
        switch ($dados->status) {
            case 0:
                $statusFoto = 'Aguardando envio';
                break;
            case 1:
                $statusFoto = 'Aprovada';
                break;
            case 2:
                $statusFoto = 'Em análise';
                break;
            default:
                $statusFoto = 'Desconhecido';
                break;
        }
        return $statusFoto;
    }

}
