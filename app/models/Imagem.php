<?php

use Intervention\Image\ImageManagerStatic as Image;

class Imagem
{

    //CARREGAR A IMAGEM
    public function carregarImagem($caminhoImagem, $widen, $ext)
    {

        if (!empty($caminhoImagem) AND file_exists($caminhoImagem)) {
            $foto = $caminhoImagem;
        } else {
            $foto = PUBLIC_ROOT . DIR_STR . 'assets' . DIR_STR . 'images' . DIR_STR . 'error' . DIR_STR . 'imagem404.png';
        }

        Image::configure(array('driver' => 'imagick'));

        $img = Image::make($foto)->widen($widen);

        return $img->response();
    }

}
