<?php

class Lote
{

    //VARIAVEIS
    private $db;

    //CONECTAR AO BANCO DE DADOS
    public function __construct()
    {
        $this->db = new Database();
    }

    //VERIFICAR SE LOTE EXISTE
    public function existeLote($loteid)
    {
        $this->db->query('SELECT * FROM ca_lotes WHERE id=:loteid');
        $this->db->bind(':loteid', $loteid);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //VERIFICAR SE UM LOTE X PERTENCE A UM CURSO
    public function possoVerLote($dados)
    {
        $this->db->query('SELECT * FROM ca_lotes WHERE id=:loteid AND cursid=:cursid');
        $this->db->bind(':loteid', $dados['loteid']);
        $this->db->bind(':cursid', $dados['cursid']);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //ADD NOVO LOTE
    public function addLote($dados)
    {
        $this->db->query('INSERT INTO ca_lotes (cursid, total, dataregistro) VALUES (:cursid, :total, :dataregistro)');
        $this->db->bind(':cursid', $dados['cursid']);
        $this->db->bind(':total', $dados['total']);
        $this->db->bind(':dataregistro', time());
        if ($this->db->execute()) {
            return $this->db->lastInsertId();
        } else {
            return FALSE;
        }
    }

    //RETORNAR TODOS OS LOTES DE UM CURSO
    public function meusLotesGerados($curso)
    {
        $this->db->query('SELECT lote.id AS loteid, lote.cursid, lote.total AS total_carteirinhas, lote.dataregistro, COUNT(cart.id) AS total_carteiras_geradas FROM ca_carteiras cart RIGHT OUTER JOIN ca_lotes lote ON cart.loteid = lote.id WHERE lote.cursid = :cursid GROUP BY lote.id, lote.cursid, lote.total, lote.dataregistro ORDER BY lote.dataregistro');
        $this->db->bind(':cursid', $curso);
        return $this->db->resultSet();
    }

    //RETORNAR INFORMAÇÕES DE UM LOTE
    public function infoLote($id)
    {
        $this->db->query('SELECT lotes.id AS loteid, lotes.cursid, ca_cursos.nome AS nome_curso, lotes.dataregistro, lotes.total AS total_carteirinhas FROM ca_lotes lotes INNER JOIN ca_carteiras carteira ON carteira.loteid = lotes.id INNER JOIN ca_cursos ON lotes.cursid = ca_cursos.id WHERE lotes.id = :id GROUP BY ca_cursos.nome');
        $this->db->bind(':id', $id);
        $dados = $this->db->resultSet();
        return array_shift($dados);
    }

    public function precoLoteCarteirinhas($totalCarteiras)
    {
        return number_format($totalCarteiras, 2, ",", ".");
    }

}
