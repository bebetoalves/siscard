<?php

class Matricula
{

    // VARIAVEIS
    private $db;

    // CONECTAR AO BANCO DE DADOS
    public function __construct()
    {
        $this->db = new Database();
    }

    //VERIFICAR SE EXISTE MATRICULA NO BANCO
    public function existeMatricula($matricula)
    {
        $this->db->query('SELECT * FROM ca_matriculas WHERE matricula=:matricula');
        $this->db->bind(':matricula', $matricula);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //TOTAL ALUNOS CURSO
    public function totalMatriculasCurso($curso)
    {
        $this->db->query("SELECT * FROM ca_matriculas WHERE cursid = :cursid");
        $this->db->bind(":cursid", $curso);
        $this->db->execute();

        return $this->db->rowCount();
    }

    //RETORNAR O CURSO DA MATRICULA
    public function pegarCursoMatricula($matricula)
    {
        $this->db->query('SELECT * FROM ca_matriculas WHERE matricula=:matricula');
        $this->db->bind(':matricula', $matricula);
        return $this->db->single()->cursid;
    }

    //RETORNAR TODOS OS DADOS DE UMA MATRÍCULA
    public function dadosMatricula($matricula)
    {
        $this->db->query('SELECT m.id, m.matricula, m.nome, m.cursid, c.nome AS nome_curso FROM ca_matriculas AS m JOIN ca_cursos AS c ON m.cursid = c.id WHERE m.matricula=:matricula');
        $this->db->bind(':matricula', $matricula);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            $dados = $this->db->resultSet();
            return array_shift($dados);
        } else {
            return FALSE;
        }
    }

    //FUNÇÃO PARA VERIFICAR SE OS CADASTROS ESTÃO ABERTOS PARA UMA MATRICULA
    public function cadastrosAbertos($matricula)
    {
        $this->db->query('SELECT c.cadastros AS status_cadastro FROM ca_cursos AS c JOIN ca_matriculas AS mat ON c.id = mat.cursid WHERE mat.matricula=:matricula');
        $this->db->bind(':matricula', $matricula);
        $this->db->execute();

        if ($this->db->rowCount() > 0) {
            if ($this->db->single()->status_cadastro == 1) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

}
