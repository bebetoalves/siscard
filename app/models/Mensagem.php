<?php

class Mensagem
{

    //VARIAVEIS
    private $db;

    //CONECTAR AO BANCO DE DADOS
    public function __construct()
    {
        $this->db = new Database();
    }

    //VERIFICAR SE A MENSAGEM EXISTE
    public function verificarExistencia($id)
    {
        $this->db->query('SELECT * FROM ca_mensagens WHERE id=:id');
        $this->db->bind(':id', $id);
        $single = $this->db->single();
        if (empty($single)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //RETORNAR TODOS OS DADOS DA MENSAGEM PELO CAID
    public function mensagensPorCAID($caid)
    {
        $this->db->query('SELECT * FROM ca_mensagens WHERE caid=:caid');
        $this->db->bind(':caid', $caid);
        return $this->db->resultSet();
    }

    //RETORNAR TODOS OS DADOS DA MENSAGEM PELO CAID
    public function dadosMensagemPorID($msid)
    {
        $this->db->query('SELECT * FROM ca_mensagens WHERE id=:id');
        $this->db->bind(':id', $msid);
        return array_shift($this->db->resultSet());
    }
}