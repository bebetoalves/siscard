<?php

class Sessao
{
    //VARIAVEIS
    private $db;

    //CONECTAR AO BANCO DE DADOS
    public function __construct()
    {
        $this->db = new Database();
    }

    //CRIAR SESSAO Do ALUNO
    public function novaSessao($nome, $dado)
    {
        //VERIFICAR SE A SESSAO EXISTE
        if (isset($_SESSION[$nome])) {
            $this->apagarSessao($nome);
        }
        return $_SESSION[$nome] = $dado;
    }

    //ATUALIZAR SESSAO
    public function atualizarSessao($nome, $dado)
    {
        if (isset($_SESSION[$nome])) {
            return $_SESSION[$nome] = $dado;
        }
    }

    //APAGAR SESSAO DO ALUNO
    public function apagarSessao($nome)
    {
        unset($_SESSION[$nome]);
    }

    //VERIFICAR SE EXISTE SESSÃO DO ALUNO
    public function existeSessao($nome)
    {
        if (isset($_SESSION[$nome])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
