<?php
require_once APP_ROOT . '/views/partials/cadmin/header.php';
?>

<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-5 col-sm-5 col-12">
                <h5 class="font-weight-light">Olá, <span class="font-weight-bold"><?= $meusDados->email; ?></span>!
                    <br/>
                    Bem-vindo ao Painel Administrativo do Siscard!
                </h5>
            </div>
            <div class="col-lg-6 col-md-7 col-sm-7 col-12">
                <span class="nomecurso float-right">
                    <?= tratarNomeCompleto($infoMeuCurso->nome); ?>
                </span>
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-end">
    <div class="col-md-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title text-md-center text-xl-left">Alunos Matrículados</p>
                <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                    <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0"><?= $cards['totalMatriculasCurso']; ?></h3>
                    <i class="ti-pie-chart icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
                <div class="mb-3"></div>
            </div>
        </div>
    </div>
    <div class="col-md-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title text-md-center text-xl-left">Alunos Cadastrados</p>
                <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                    <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0"><?= $cards['totalAlunosCadastrados']; ?></h3>
                    <i class="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title text-md-center text-xl-left">Carteirinhas Geradas</p>
                <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                    <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0"><?= $cards['totalCarteirasGeradas']; ?></h3>
                    <i class="ti-id-badge icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title text-md-center text-xl-left">Cadastros</p>
                <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                    <h4 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0 <?= ($cards['statusCadastro'] == 1) ? 'text-success' : 'text-danger'; ?>">
                        <?= ($cards['statusCadastro'] == 1) ? 'Abertos' : 'Fechados'; ?>
                    </h4>
                    <i class="ti-pencil-alt icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mt-3 mb-5">
    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Carteirinhas Geradas</h4>
                <?= $chartCarteirinhas; ?>
            </div>
        </div>
    </div>


    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Alunos Cadastrados </h4>
                <?= $chartAlunos; ?>
            </div>
        </div>
    </div>
</div>
<?php
require_once APP_ROOT . '/views/partials/cadmin/footer.php';
?>

