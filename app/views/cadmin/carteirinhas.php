<?php
require_once APP_ROOT . '/views/partials/cadmin/header.php';
?>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Carteirinhas</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" aria-current="page">
                                <a href="<?= URL_ROOT; ?>/capainel/index" class="breadcrumb-text">
                                    Painel de Controle
                                </a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Carteirinhas</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

<?= flash('carteirinhas'); ?>

    <!-- GERAR LOTE DE CARTEIRINHAS -->
    <div class="container-scroller">

        <div class="card">
            <div class="card-header">
                Novo Lote
            </div>
            <div class="card-body">
                <ul class="list-arrow">
                    <li>
                        Para gerar um lote será necessário ter ao menos <strong>1</strong> aluno com o status de foto e
                        pagamento aprovados.
                    </li>
                    <li>
                        <?php if ($dadosCarteirinhas['totalAlunosProntos'] == 0) : ?>
                            No momento há <strong>zero</strong> carteirinhas prontas para serem geradas.
                        <?php elseif ($dadosCarteirinhas['totalAlunosProntos'] == 1) : ?>
                            No momento há <strong>1</strong> carteirinha pronta para ser gerada.
                        <?php else : ?>
                            No momento há
                            <strong><?= $dadosCarteirinhas['totalAlunosProntos']; ?></strong> alunos com status de foto e pagamento aprovados.
                        <?php endif; ?>
                    </li>
                </ul>

                <div class="row mt-3">
                    <div class="col-md-3">
                        <?php if ($dadosCarteirinhas['totalAlunosProntos'] >= 1) : ?>
                            <button type="button" class="btn btn-primary btn-block" data-toggle="modal"
                                    data-target="#gerarLote">
                                GERAR LOTE
                            </button>
                        <?php else : ?>
                            <button type="button" class="btn btn-primary cursor-blocked btn-block" disabled>
                                GERAR LOTE
                            </button>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php if ($dadosCarteirinhas['totalAlunosProntos'] >= 1) : ?>
    <!-- MODAL GERAÇÃO DO LOTE -->
    <div class="modal fade" id="gerarLote" tabindex="-1" role="dialog" aria-labelledby="gerarLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gerarLabel">Verificação de Segurança</h4>
                </div>
                <div class="modal-body">
                    <p class="lead">
                        O novo lote terá um total de <b><?= $dadosCarteirinhas['totalNovoLote']; ?></b> carteirinhas, ao
                        preço de <b>R$ <?= $dadosCarteirinhas['precoLoteCarteirinhas']; ?></b> que deverá ser pago para
                        a <b>Include Jr</b>.
                    </p>
                    <br/>
                    <p class="lead">
                        Deseja realmente gerar um novo lote de carteirinhas?
                    </p>

                </div>
                <div class="modal-footer">
                    <a href="<?= URL_ROOT; ?>/capainel/novolote" class="btn btn-success">CONFIRMAR</a>
                    <button type="button" class="btn btn-light" data-dismiss="modal">CANCELAR</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

    <div class="my-5"></div>

    <!-- LISTAR LOTES ANTIGOS -->
    <div class="container-scroller">

        <div class="card">
            <div class="card-header">
                Lotes Gerados
            </div>
            <div class="card-body">
                <div class="row pt-3">
                    <div class="col-12">
                        <?php if (count($dadosCarteirinhas['meusLotes']) > 0) : ?>
                            <div class="table-responsive">
                                <table id="lotes-gerados" class="table">
                                    <thead>
                                    <tr class="text-center">
                                        <th>Lote</th>
                                        <th>Data de Criação</th>
                                        <th>Total de Carteirinhas</th>
                                        <th>Status</th>
                                        <th>Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($dadosCarteirinhas['meusLotes'] as $lote) : ?>
                                        <tr class="text-center">
                                            <td>
                                                <i class="fas fa-hashtag"></i> <?= $lote->loteid; ?>
                                            </td>
                                            <td>
                                                <?= date('d/m/Y - H:i:s', $lote->dataregistro); ?>
                                            </td>
                                            <td>
                                                <?= $lote->total_carteirinhas; ?>
                                            </td>
                                            <td>
                                                <div class="text-success">
                                                    <i class="fas fa-check"></i> Completo
                                                </div>
                                            </td>
                                            <td>
                                                <a href="<?= URL_ROOT; ?>/capainel/verlote/<?= $lote->loteid; ?>"
                                                   class="btn btn-success py-2">
                                                    <i class="fas fa-print"></i> Imprimir
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php else : ?>
                            <div class="lead mb-3">
                                Não há lotes gerados no momento!
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
require_once APP_ROOT . '/views/partials/cadmin/footer.php';
?>