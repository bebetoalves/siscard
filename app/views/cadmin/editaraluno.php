<?php
require_once APP_ROOT . '/views/partials/cadmin/header.php';
?>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Editar Aluno</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" aria-current="page">
                                <a href="<?= URL_ROOT; ?>/capainel/index" class="breadcrumb-text">
                                    Painel de Controle
                                </a>
                            </li>
                            <li class="breadcrumb-item" aria-current="page">
                                <a href="<?= URL_ROOT; ?>/capainel/geriralunos/<?= $pagina; ?>" class="breadcrumb-text">
                                    Gerir Alunos
                                </a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Editar Aluno</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

<?= flash('editaraluno'); ?>


<?php if ($tenhoCarteira): ?>
    <div class="alert alert-warning" role="alert">
        <h4 class="alert-heading">Atenção!</h4>
        <p>
            Esse aluno já tem uma carteirinha gerada,
            você pode encontrá-la no <a href="<?= URL_ROOT; ?>/capainel/verlote/<?= $minhaCarteira->loteid ?>#<?= $dadosAluno->matricula; ?>">
                <strong>Lote N° <?= $minhaCarteira->loteid; ?></strong></a>.
        </p>
        </div>
<?php endif; ?>

    <div class="row mb-3">
        <div class="col-lg-7 mb-3">
            <!-- CARD APROVAÇÃO/REPROVAÇÃO DE FOTO -->
            <div class="card">
                <div class="card-header">
                    Avaliação de Foto
                </div>
                <div class="card-body">
                    <?php if ($dadosFoto->status == 0): ?>
                        <div class="lead">
                            Esse aluno(a) ainda não enviou uma foto!
                        </div>
                    <?php else: ?>

                        <div class="row justify-content-start align-items-center">
                            <div class="col-md-5 text-center">
                                <figure class="figure">
                                    <img src="<?= URL_ROOT ?>/imagens/fotosalunos/<?= trim($dadosFoto->foto, '.jpg'); ?>"
                                         class="figure-img img-thumbnail img-responsive">

                                    <figcaption class="figure-caption font-weight-normal text-black">
                                        <?php if ($dadosFoto->status == 1): ?>
                                            <div class="text-success">
                                                <i class="far fa-thumbs-up"></i> Aprovada
                                            </div>
                                        <?php elseif ($dadosFoto->status == 2): ?>
                                            <div class="text-secondary">
                                                <i class="far fa-clock"></i> Aguardando Análise
                                            </div>
                                        <?php endif; ?>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-md-7">
                                <form action="<?= URL_ROOT; ?>/capainel/validarfoto/<?= $dadosAluno->id; ?><?= is_numeric($pagina) ? '/' . $pagina : ''; ?>"
                                      method="POST">
                                    <p class="card-text">
                                        Como você avalia essa foto?
                                    </p>
                                    <div class="form-group required">
                                        <label class="control-label custom-label">Ação </label>
                                        <select id="acaofoto" name="acaofoto"
                                                class="form-control <?php (!empty($formError['acaofoto'])) ? print 'is-invalid' : ''; ?>">
                                            <option selected disabled>Selecione...</option>
                                            <option value="ok">Aprovo</option>
                                            <option value="off">Reprovo</option>
                                        </select>
                                        <?php if (!empty($formError['acaofoto'])): ?>
                                            <div class="text-invalid">
                                                <?= $formError['acaofoto']; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>

                                    <div id="motivo" class="form-group required" style="display: none;">
                                        <label class="control-label custom-label">Motivo <small>(Por que será
                                                reprovada?)</small> </label>
                                        <textarea value="<?= $formData['motivo']; ?>" name="motivo"
                                                  class="form-control <?php (!empty($formError['nome'])) ? print 'is-invalid' : ''; ?>"
                                                  autocomplete="off"></textarea>

                                        <?php if (!empty($formError['motivo'])): ?>
                                            <div class="text-invalid">
                                                <?= $formError['motivo']; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <button class="btn btn-primary btn-block" type="submit">CONFIRMAR</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    <?php endif; ?>
                </div>
            </div>
        </div>


        <div class="col-lg-5">
            <!-- CARD MUDANÇA STATUS DE PAGAMENTO -->
            <div class="card">
                <div class="card-header">
                    Atualização de Pagamento
                </div>
                <div class="card-body">
                    <form id="formpagamento"
                          action="<?= URL_ROOT; ?>/capainel/atualizarpagamento/<?= $dadosAluno->id; ?><?= is_numeric($pagina) ? '/' . $pagina : ''; ?>"
                          method="POST">
                        <p class="card-text mb-3">
                            Clique no botão abaixo para atualizar o pagamento desse aluno(a).
                        </p>
                        <div class="form-group required text-center">
                            <input id="acaopagamento" name="acaopagamento" value="ok" class="form-check-input"
                                   data-on="Pago" data-off="Não pago" type="checkbox" data-toggle="toggle"
                                   data-size="sm" data-onstyle="success"
                                   data-offstyle="danger" <?= $dadosAluno->status_pagamento == 1 ? 'checked' : ''; ?>>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- CARD ATUALIZAÇÃO CADASTRAL -->
    <div class="card">
        <div class="card-header">
            Dados do Aluno
        </div>
        <div class="card-body">
            <form action="<?= URL_ROOT; ?>/capainel/editaraluno/<?= $dadosAluno->id; ?><?= is_numeric($pagina) ? '/' . $pagina : ''; ?>"
                  method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label custom-label">Nome Completo </label>
                            <input value="<?= $dadosAluno->nome ?>" type="text" id="nome" name="nome"
                                   class="form-control <?php (!empty($formError['nome'])) ? print 'is-invalid' : ''; ?>"
                                   autocomplete="off" required>
                            <?php if (!empty($formError['nome'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['nome']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label custom-label">Matrícula </label>
                            <input value="<?= $dadosAluno->matricula ?>" type="text" id="matricula"
                                   class="form-control <?php (!empty($formError['matricula'])) ? print 'is-invalid' : ''; ?>"
                                   autocomplete="off" disabled>
                            <?php if (!empty($formError['matricula'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['matricula']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group required">
                            <label class="control-label custom-label">Data de Nascimento </label>
                            <input value="<?= $dadosAluno->nascimento ?>" type="text" id="nascimento" name="nascimento"
                                   class="form-control <?php (!empty($formError['nascimento'])) ? print 'is-invalid' : ''; ?>"
                                   autocomplete="off" required>
                            <?php if (!empty($formError['nascimento'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['nascimento']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group required">
                            <label class="control-label custom-label">CPF </label>
                            <input value="<?= $dadosAluno->cpf ?>" type="text" id="cpf" name="cpf"
                                   class="form-control <?php (!empty($formError['cpf'])) ? print 'is-invalid' : ''; ?>"
                                   autocomplete="off" required>
                            <?php if (!empty($formError['cpf'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['cpf']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group required">
                            <label class="control-label custom-label">RG </label>
                            <input value="<?= $dadosAluno->rg ?>" type="text" id="rg" name="rg"
                                   class="form-control <?php (!empty($formError['rg'])) ? print 'is-invalid' : ''; ?>"
                                   autocomplete="off" required>
                            <?php if (!empty($formError['rg'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['rg']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group required">
                            <label class="control-label custom-label">E-mail </label>
                            <input value="<?= $dadosAluno->email ?>" type="email" id="email" name="email"
                                   class="form-control <?php (!empty($formError['email'])) ? print 'is-invalid' : ''; ?>"
                                   autocomplete="off" required>
                            <?php if (!empty($formError['email'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['email']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group required">
                            <label class="custom-label">Foto <small>(CAMPO OPCIONAL)</small></label>
                            <input type="file" name="foto" class="file-upload-default">
                            <div class="input-group col-xs-12">
                                <input type="text"
                                       class="form-control file-upload-info <?php (!empty($formError['foto'])) ? print 'is-invalid' : ''; ?>"
                                       disabled="" placeholder="Selecione uma foto...">
                                <span class="input-group-append">
                                <button class="file-upload-browse btn btn-primary" type="button">SELECIONAR</button>
                            </span>
                                <?php if (!empty($formError['foto'])): ?>
                                    <div class="text-invalid">
                                        <?= $formError['foto']; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <button class="btn btn-primary btn-block" type="submit">ATUALIZAR</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php
require_once APP_ROOT . '/views/partials/cadmin/footer.php';
?>