<?php
require_once APP_ROOT . '/views/partials/cadmin/header.php';
?>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Gerir Alunos</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" aria-current="page">
                                <a href="<?= URL_ROOT; ?>/capainel/index" class="breadcrumb-text">
                                    Painel de Controle
                                </a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Gerir Alunos</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

<?= flash('geriralunos'); ?>

    <div class="container-scroller">

        <div class="card">
            <div class="card-header">
                Relação de Alunos
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover text-center">
                        <thead>
                        <tr>
                            <th>Foto</th>
                            <th>Matrícula</th>
                            <th>Nome Completo</th>
                            <th>Status do Pagamento</th>
                            <th>Carteirinha Gerada</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($todosAlunos as $aluno): ?>
                            <tr>
                                <td>
                                    <figure class="figure">
                                        <?php if (empty($aluno->foto)): ?>
                                            <img src="<?= URL_ROOT; ?>/imagens/fotosalunos/avatar"
                                                 class="figure-img img-thumbnail img-responsive">
                                        <?php else: ?>
                                            <img src="<?= URL_ROOT ?>/imagens/fotosalunos/<?= trim($aluno->foto, '.jpg'); ?>"
                                                 class="figure-img img-thumbnail img-responsive">
                                        <?php endif; ?>
                                        <figcaption class="figure-caption font-weight-normal text-black">
                                            <?php if ($aluno->foto_status == 0): ?>
                                                <div class="text-secondary">
                                                    <i class="fas fa-user-clock"></i> Aguardando Envio
                                                </div>
                                            <?php elseif ($aluno->foto_status == 1): ?>
                                                <div class="text-success">
                                                    <i class="far fa-thumbs-up"></i> Aprovada
                                                </div>
                                            <?php elseif ($aluno->foto_status == 2): ?>
                                                <div class="text-secondary">
                                                    <i class="far fa-clock"></i> Aguardando Análise
                                                </div>
                                            <?php endif; ?>
                                        </figcaption>
                                    </figure>
                                </td>
                                <td><?= $aluno->matricula; ?></td>
                                <td><?= wordwrap($aluno->nome, 20, '<br />'); ?></td>
                                <td><?= ($aluno->status_pagamento == 0 ? 'Aguardando Pagamento' : 'Pago'); ?></td>
                                <td><?= $aluno->total_carteirinhas == 0 ? '<strong class="text-danger">NÃO</strong>' : '<strong class="text-success">SIM</strong>'?></td>
                                <td>
                                    <div class="btn-group-sm">
                                        <a href="<?= URL_ROOT; ?>/capainel/editaraluno/<?= $aluno->aluid; ?>/<?= $paginacao['paginaAtual']; ?>"
                                           class="btn btn-sm btn-primary"><i class="fas fa-user-edit"></i> Editar</a>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="mt-3">
                        <?= $paginacao['paginador']; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
require_once APP_ROOT . '/views/partials/cadmin/footer.php';
?>