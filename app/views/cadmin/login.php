<?php
require_once APP_ROOT . '/views/partials/pages/header.php';
?>
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth px-0">
            <div class="row w-100 mx-0">
                <div class="col-lg-4 mx-auto">
                    <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                        <div class="brand-logo">
                            <img src="<?= URL_ROOT; ?>/assets/images/logos/logo_siscard.png" alt="logo">
                        </div>
                        <h4>Seja bem-vindo(a)!</h4>
                        <h6 class="font-weight-light">Faça o login para continuar.</h6>

                        <?= flash('cadmin_login'); ?>

                        <form action="<?= URL_ROOT; ?>/cadmin/login" method="POST" class="pt-3">
                            <div class="form-group required">
                                <label class="control-label custom-label">E-mail </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent border-right-0 <?php (!empty($formError['email'])) ? print 'is-invalid' : ''; ?>">
                                        <i class="ti-user text-primary"></i>
                                    </span>
                                    </div>
                                    <input type="text"
                                           <?php if (!empty($formData['email'])): ?>value="<?= $formData['email']; ?>"<?php endif; ?>
                                           name="email" <?php if (isset($_SESSION['ca_lembrarme'])): ?>
                                           value="<?= $_SESSION['ca_lembrarme'] ?>"
                                           <?php endif; ?>class="form-control form-control-lg border-left-0 <?php (!empty($formError['email'])) ? print 'is-invalid' : ''; ?>"
                                           placeholder="E-mail" autocomplete="off"/>
                                </div>
                                <?php if (!empty($formError['email'])): ?>
                                    <div class="text-invalid">
                                        <?= $formError['email']; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="form-group required">
                                <label class="control-label custom-label">Senha </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent border-right-0 <?php (!empty($formError['senha'])) ? print 'is-invalid' : ''; ?>">
                                        <i class="ti-lock text-primary"></i>
                                    </span>
                                    </div>
                                    <input type="password" name="senha"
                                           class="form-control form-control-lg border-left-0 <?php (!empty($formError['senha'])) ? print 'is-invalid' : ''; ?>"
                                           placeholder="Senha"/>
                                </div>
                                <?php if (!empty($formError['senha'])): ?>
                                    <div class="text-invalid">
                                        <?= $formError['senha']; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="my-2 d-flex justify-content-between align-items-center">
                                <div class="form-check">
                                    <label class="form-check-label text-muted">
                                        <input name="ca_lembrarme" type="checkbox" class="form-check-input">
                                        Lembrar-me
                                    </label>
                                </div>
                            </div>
                            <div class="mt-3">
                                <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">
                                    LOGIN
                                </button>
                            </div>
                            <h6 class="text-center mt-4 font-weight-light">
                                Não tem uma conta?
                                <br/>
                                Entre em contato com a <a href="http://includejr.com.br" target="_blank">
                                    <img src="<?= URL_ROOT; ?>/assets/images/logos/include_logo_colorido.png"
                                         class="include_logo_alt align-text-top"/>
                                </a>
                            </h6>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
<?php
require_once APP_ROOT . '/views/partials/pages/footer.php';
?>