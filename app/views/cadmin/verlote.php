<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>
        <?= $tituloDaPagina; ?>
    </title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script>
        $(function () {
            $(this).bind("contextmenu", function (e) {
                e.preventDefault();
            });
        });

        (function ($) {
            $.fn.disableSelection = function () {
                return this
                    .attr('unselectable', 'on')
                    .css('user-select', 'none')
                    .on('selectstart dragstart', false);
            };
        })(jQuery);
    </script>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Oswald:400&display=swap');
        @import url('https://fonts.googleapis.com/css?family=Source+Code+Pro:400,600&display=swap');
        @import url('https://fonts.googleapis.com/css?family=Oxygen:400,700&display=swap');

        @page {
            size: A4
        }

        * {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            -webkit-touch-callout: none; /* iOS Safari */
            -webkit-user-select: none; /* Chrome/Safari/Opera */
            -khtml-user-select: none; /* Konqueror */
            -moz-user-select: none; /* Firefox */
            -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none;
        }

        @media print {
            body, page {
                margin: 0;
                box-shadow: unset;
            }

            * {
                -webkit-print-color-adjust: exact !important; /*Chrome, Safari */
                color-adjust: exact !important; /*Firefox*/
            }

            page, table {
                page-break-inside: avoid;
            }
        }

        .cart-table-body {
            border-spacing: 1px;
            border-width: 1px;
            border-color: gray;
            border-style: dotted;
            border-collapse: inherit;
            margin-bottom: 10px;
            position: relative;
            z-index: 0;
        }

        .cart-background {
            background-image: url("../../assets/images/carteiras/cart.png");
            background-repeat: no-repeat;
            background-size: 675px auto;
            width: 675px;
            height: 225px;
            z-index: 1;
        }

        ul {
            list-style: none;
        }

        .final {
            top: 50%;
            bottom: 50%;
        }

        @page {
            size: A4
        }

        .frontpage {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .frontpage > * {
            text-align: center;
        }

        .frontpage footer {
            align-self: flex-end;
        }

        .box {
            height: 50%;
            display: inline-block;
            white-space: nowrap;
            float: left;
            position: relative;
            left: 18px;
            bottom: 2px;
        }

        .cabecalho {
            height: 20%;
            display: inline-block;
            white-space: nowrap;
            float: left;
            position: absolute;
            top: 6px;
            right: 358px;
        }

        .emissao_validade {
            height: 10%;
            float: left;
            position: absolute;
            top: 179px;
            left: 20px;
        }

        .info_ve {
            text-align: left;
            font-family: 'Oxygen', sans-serif;
            font-weight: normal;
            font-size: 11px;
            text-transform: uppercase;
            display: inline-block;
            white-space: normal;
            vertical-align: middle;
        }

        .identificacao {
            text-align: left;
            font-family: 'Oswald', sans-serif;
            font-weight: bold;
            font-size: 15px;
            text-transform: uppercase;
            display: inline-block;
            white-space: normal;
        }

        .logoufc {
            display: inline-block;
            white-space: normal;
            vertical-align: middle;
            margin-right: 2em;
        }

        .infoaluno {
            text-align: left;
            font-family: 'Oxygen', sans-serif;
            font-weight: normal;
            font-size: 11px;
            letter-spacing: 1px;
            text-transform: uppercase;
            display: inline-block;
            white-space: normal;
            vertical-align: middle;
            line-height: 1.4;
            padding-left: 3px;
        }

        .fotoaluno {
            display: inline-block;
            white-space: normal;
            vertical-align: middle;
        }

        .boxassinaturas {
            width: 50%;
            position: absolute;
            padding: 0px;
            margin: 0px;
            top: 1em;
            left: 50%;
        }

        .assinaturas {
            font-family: 'Oxygen', sans-serif;
            font-size: 11px;
            font-weight: normal;
        }

        .ass_cordenador {
            text-align: center;
            top: 2em;
            position: relative;
        }

        .ass_diretor {
            text-align: center;
            top: 2em;
            position: relative;
        }

        .linha {
            font-family: Arial;
            font-size: 11px;
        }


        ul {
            margin: 0px;
            padding: 0px;
        }

    </style>

    <!--    <script type="text/javascript">-->
    <!--        $(document).ready(function () {-->
    <!--            window.print();-->
    <!--        });-->
    <!--    </script>-->
</head>
<body class="A4" oncopy="return false" oncut="return false" onpaste="return false">
<section class="sheet padding-15mm frontpage">
    <ul class="final">
        <li>
            <h1>Lote N°. <?= $infoLote->loteid; ?></h1>
        </li>
        <li>
            <h1><?= $infoLote->nome_curso; ?></h1>
        </li>
        <li>Total de Carteirinhas: <b><?= $infoLote->total_carteirinhas; ?></b></li>
        <li>Preço Unitário: <b>R$ <?= PRECO_UNITARIO; ?></b></li>
        <li>Total: <b>R$ <?= $valorLote; ?></b></li>
    </ul>
</section>


<?php foreach (array_chunk($carteirasLote, 4) as $dados): ?>
    <section class="sheet padding-15mm">
        <?php foreach ($dados as $cart): ?>
            <table class="cart-table-body" align="center">
                <tr>
                    <th class="cart-background">
                        <a id="<?= $cart->matricula; ?>"></a>
                        <div class="cabecalho">
                            <!-- LOGO UFC -->
                            <div class="logoufc">
                                <img src="<?= URL_ROOT; ?>/assets/images/carteiras/logoufc.png" height="30"/>
                            </div>

                            <div class="identificacao">
                                Identificação Estudantil
                            </div>
                        </div>

                        <div class="box">
                            <div class="fotoaluno">
                                <img src="<?= URL_ROOT . '/uploads/alunos/' . $cart->foto; ?>" height="108"/>
                            </div>
                            <div class="infoaluno">
                                <ul>
                                    <li>Nome: <b><?= wordwrap($cart->nome_aluno, 25, '<br />'); ?></b>
                                    </li>

                                    <li>Curso: <b><?= $infoLote->nome_curso; ?></b></li>

                                    <li>Matrícula: <b><?= $cart->matricula; ?></b></li>

                                    <li>RG: <b><?= $cart->rg; ?></b></li>

                                    <li>CPF: <b><?= $cart->cpf; ?></b></li>

                                    <li>Data de Nascimento: <b><?= $cart->nascimento; ?></b></li>

                                </ul>
                            </div>
                        </div>

                        <div class="emissao_validade">
                            <div class="info_ve" style="margin-right: 50px">
                                DATA DE EMISSÃO: <b><?= date('d/m/Y', $infoLote->dataregistro); ?></b>
                            </div>
                            <div class="info_ve">
                                VALIDADE: <b><?= date('m/Y', strtotime('1 year', 0) + $infoLote->dataregistro); ?></b>
                            </div>
                        </div>

                        <div class="boxassinaturas">
                            <div class="assinaturas">
                                <?php if ($infoMeuCurso->ass_coordenador != NULL): ?>
                                    <div class="ass_cordenador">
                                        <img src="<?= $infoMeuCurso->ass_coordenador; ?>" height="30"/>
                                    </div>
                                <?php else: ?>
                                    <div class="ass_cordenador">
                                        <br/>
                                        <br/>
                                    </div>
                                <?php endif; ?>

                                <span class="linha">____________________________________</span>
                                <p>
                                    <b><?= $infoMeuCurso->coordenador; ?></b>
                                    <br/>
                                    <small>COORDENADOR(A) DO CURSO</small>
                                </p>
                            </div>

                            <div class="assinaturas">
                                <?php if ($infoMeuCurso->diretor == 1): ?>
                                    <div class="ass_diretor">
                                        <img src="data:image/png;base64,<?= $infoDiretor->assinatura; ?>" height="30"/>
                                    </div>
                                <?php else: ?>
                                    <div class="ass_diretor">
                                        <br/>
                                        <br/>
                                    </div>
                                <?php endif; ?>

                                <span class="linha">____________________________________</span>
                                <p>
                                    <b><?= $infoDiretor->nome; ?></b>
                                    <br/>
                                    <small>DIRETOR DO CAMPUS</small>
                                </p>
                            </div>
                        </div>

                    </th>
                </tr>
            </table>
        <?php endforeach; ?>
    </section>
<?php endforeach; ?>
</body>
</html>
