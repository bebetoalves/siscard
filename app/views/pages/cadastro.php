<?php
require_once APP_ROOT . '/views/partials/pages/header.php';
?>
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth px-0">
            <div class="row w-100 mx-0">
                <div class="col-lg-9 mx-auto">
                    <div class="brand-logo">
                        <a href="<?= URL_ROOT; ?>">
                            <img src="<?= URL_ROOT; ?>/assets/images/logos/logo_siscard.png" alt="logo">
                        </a>
                    </div>
                    <div class="mb-3">
                        <h4>Simples. Rápido. Fácil.</h4>
                        <p class="font-weight-light">
                            Alguns dados aqui outros ali. Não precisamos saber tudo sobre você agora. <br>
                            Faça o cadastro e obtenha acesso aos recursos básicos do Siscard!
                        </p>
                    </div>

                    <?= flash('cadastro'); ?>

                    <div class="row justify-content-center align-items-center pb-5 pt-2">
                        <div class="col-lg-12">

                            <!-- INFORMAÇÕES BÁSICAS DO ALUNO -->
                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required">
                                            <label class="control-label custom-label">Nome Completo </label>
                                            <input type="text" value="<?= $dadosMatricula->nome; ?>"
                                                   placeholder="Nome Completo" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label class="control-label custom-label">Matrícula </label>
                                            <input type="text" value="<?= $dadosMatricula->matricula; ?>"
                                                   class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label class="control-label custom-label">Curso </label>
                                            <input type="text" value="<?= $dadosMatricula->nome_curso; ?>"
                                                   class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <!-- FORMULÁRIO PARA COMPLETAR O CADASTRO -->
                            <form action="<?= URL_ROOT; ?>/cadastro/formulario/<?= $dadosMatricula->matricula; ?>"
                                  method="POST">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group required">
                                            <label class="control-label custom-label">CPF </label>
                                            <input type="text" id="cpf" name="cpf"
                                                   value="<?= isset($formData['cpf']) ? $formData['cpf'] : ''; ?>"
                                                   placeholder="N° do seu CPF"
                                                   class="form-control <?php (!empty($formError['cpf'])) ? print 'is-invalid' : ''; ?>"
                                                   autocomplete="off" required>
                                            <?php if (!empty($formError['cpf'])): ?>
                                                <div class="text-invalid">
                                                    <?= $formError['cpf']; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group required">
                                            <label class="control-label custom-label">RG </label>
                                            <input type="text" name="rg" id="rg" maxlength="14"
                                                   value="<?= isset($formData['rg']) ? $formData['rg'] : ''; ?>"
                                                   placeholder="N° do seu RG"
                                                   class="form-control <?php (!empty($formError['rg'])) ? print 'is-invalid' : ''; ?>"
                                                   autocomplete="off" required>
                                            <?php if (!empty($formError['rg'])): ?>
                                                <div class="text-invalid">
                                                    <?= $formError['rg']; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group required">
                                            <label class="control-label custom-label">Data de Nascimento </label>
                                            <input type="text" name="nascimento"
                                                   value="<?= isset($formData['nascimento']) ? $formData['nascimento'] : ''; ?>"
                                                   placeholder="DD/MM/AAAA" id="nascimento"
                                                   class="form-control <?php (!empty($formError['nascimento'])) ? print 'is-invalid' : ''; ?>"
                                                   autocomplete="off" required>
                                            <?php if (!empty($formError['nascimento'])): ?>
                                                <div class="text-invalid">
                                                    <?= $formError['nascimento']; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required">
                                            <label class="control-label custom-label">Senha </label>
                                            <input type="password" name="senha" placeholder="Digite sua senha"
                                                   class="form-control <?php (!empty($formError['senha'])) ? print 'is-invalid' : ''; ?>"
                                                   autocomplete="off" required>
                                            <?php if (!empty($formError['senha'])): ?>
                                                <div class="text-invalid">
                                                    <?= $formError['senha']; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required">
                                            <label class="control-label custom-label">Confirme sua Senha </label>
                                            <input type="password" name="csenha" placeholder="Confirme sua senha"
                                                   class="form-control <?php (!empty($formError['csenha'])) ? print 'is-invalid' : ''; ?>"
                                                   autocomplete="off" required>
                                            <?php if (!empty($formError['csenha'])): ?>
                                                <div class="text-invalid">
                                                    <?= $formError['csenha']; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required">
                                            <label class="control-label custom-label">E-mail </label>
                                            <input type="email" name="email"
                                                   value="<?= isset($formData['email']) ? $formData['email'] : ''; ?>"
                                                   placeholder="Digite seu endereço de e-mail"
                                                   class="form-control <?php (!empty($formError['email'])) ? print 'is-invalid' : ''; ?>"
                                                   autocomplete="off" required>
                                            <?php if (!empty($formError['email'])): ?>
                                                <div class="text-invalid">
                                                    <?= $formError['email']; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row justify-content-start">
                                    <div class="col-md-3">
                                        <button class="btn btn-success btn-block" type="submit">CONFIRMAR CADASTRO
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer mt-0">
        <div class="text-center">
            <span class="align-middle">SISCARD &copy; 2019 — Desenvolvido por</span>
            <a href="https://www.includejr.com.br/" target="_blank">
                <img src="<?= URL_ROOT; ?>/assets/images/logos/include_logo.png" class="include_logo"/>
            </a>
        </div>
    </div>
<?php
require_once APP_ROOT . '/views/partials/pages/footer.php';
?>