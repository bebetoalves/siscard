<?php
require_once APP_ROOT . '/views/partials/pages/header.php';
?>
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
            <div class="row flex-grow">
                <div class="col-lg-6 d-flex align-items-center justify-content-center">
                    <div class="auth-form-transparent text-left p-3">
                        <div class="brand-logo">
                            <a href="<?= URL_ROOT; ?>">
                                <img src="<?= URL_ROOT; ?>/assets/images/logos/logo_siscard.png" alt="logo">
                            </a>
                        </div>
                        <h4>Área de Login</h4>
                        <p class="font-weight-light">
                            Bem-vindo ao Sistema de Carteiras Estudantis!
                        </p>
                        <?= flash('login'); ?>
                        <form method="POST" action="<?= URL_ROOT; ?>/login/" class="pt-3">
                            <div class="form-group required">
                                <label class="control-label custom-label">Passe </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent border-right-0 <?php (!empty($formError['passe'])) ? print 'is-invalid' : ''; ?>">
                                        <i class="ti-user text-primary"></i>
                                    </span>
                                    </div>
                                    <input type="text"
                                           <?php if (!empty($formData['passe'])): ?>value="<?= $formData['passe']; ?>"<?php endif; ?>
                                           name="passe" <?php if (isset($_SESSION['lembrarme'])): ?>
                                           value="<?= $_SESSION['lembrarme'] ?>"
                                           <?php endif; ?>class="form-control form-control-lg border-left-0 <?php (!empty($formError['passe'])) ? print 'is-invalid' : ''; ?>"
                                           placeholder="E-mail ou matrícula" autocomplete="off"/>
                                </div>
                                <?php if (!empty($formError['passe'])): ?>
                                    <div class="text-invalid">
                                        <?= $formError['passe']; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="form-group required">
                                <label class="control-label custom-label">Senha </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent border-right-0 <?php (!empty($formError['senha'])) ? print 'is-invalid' : ''; ?>">
                                        <i class="ti-lock text-primary"></i>
                                    </span>
                                    </div>
                                    <input type="password" name="senha"
                                           class="form-control form-control-lg border-left-0 <?php (!empty($formError['senha'])) ? print 'is-invalid' : ''; ?>"
                                           placeholder="Senha"/>
                                </div>
                                <?php if (!empty($formError['senha'])): ?>
                                    <div class="text-invalid">
                                        <?= $formError['senha']; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="form-check">
                                    <label class="form-check-label text-muted">
                                        <input name="lembrarme" type="checkbox" class="form-check-input">
                                        Lembrar-me
                                    </label>
                                </div>
                                <a href="<?= URL_ROOT; ?>/redefinirsenha" class="auth-link text-black">Esqueceu a
                                    senha?</a>
                            </div>
                            <div class="my-3">
                                <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"
                                        type="submit">FAZER LOGIN
                                </button>
                            </div>
                        </form>
                        <div class="text-center font-weight-light cadastro-custom-margin">
                            Ainda não faz parte? <a href="<?= URL_ROOT; ?>/cadastro/"
                                                    class="text-primary">Cadastre-se!</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 login-half-bg d-flex flex-row">
                    <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
                        <h2 class="semi-bold text-white font-weight-bold roboto-condensed">
                            Novo. Seguro. Diferente.</h2>
                        <p class="text-white font-weight-light roboto-condensed">
                            Com o Siscard ficou fácil ter a sua carteira estudantil e aproveitar as vantagens. <br/>
                            O que está esperando? Cadastre-se agora mesmo!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
require_once APP_ROOT . '/views/partials/pages/footer.php';
?>