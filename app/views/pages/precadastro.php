<?php
require_once APP_ROOT . '/views/partials/pages/header.php';
?>
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth px-0">
            <div class="row w-100 mx-0">
                <div class="col-lg-9 mx-auto">
                    <div class="brand-logo">
                        <a href="<?= URL_ROOT; ?>">
                            <img src="<?= URL_ROOT; ?>/assets/images/logos/logo_siscard.png" alt="logo">
                        </a>
                    </div>

                    <div class="mb-3">
                        <h4>Simples. Rápido. Fácil.</h4>
                        <p class="font-weight-light">
                            Alguns dados aqui outros ali. Não precisamos saber tudo sobre você agora. <br>
                            Faça o cadastro e obtenha acesso aos recursos básicos do Siscard!
                        </p>
                    </div>

                    <div class="row justify-content-center align-items-center">
                        <div class="col-sm-8">
                            <div class="mb-4 mt-3">
                                <div class="display-3 roboto-condensed">
                                    Vamos começar?
                                </div>
                                <p class="lead">
                                    Comece com sua matrícula, com ela teremos acesso a algumas informações básicas sobre
                                    você!
                                </p>
                            </div>

                            <?= flash('precadastro'); ?>

                            <form action="<?= URL_ROOT; ?>/cadastro" method="POST">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required">
                                            <label class="control-label custom-label">Matrícula </label>
                                            <input type="text" id="matricula" maxlength="6" name="matricula"
                                                   placeholder=""
                                                   class="form-control <?php (!empty($formError['matricula'])) ? print 'is-invalid' : ''; ?>"
                                                   autocomplete="off">
                                            <?php if (!empty($formError['matricula'])): ?>
                                                <div class="text-invalid">
                                                    <?= $formError['matricula']; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-block" type="submit">PROSSEGUIR</button>
                            </form>
                            <div class="mt-4 font-weight-light">
                                Você já tem uma conta no Siscard? <a href="<?= URL_ROOT; ?>"
                                                                     class="text-primary">Login</a>
                            </div>
                        </div>
                        <div class="d-none d-sm-block col-sm-4">
                            <img src="<?= URL_ROOT; ?>/assets/images/msgs/search.png"
                                 class="img-fluid mx-auto center-block"/>
                        </div>
                    </div>

                    <div class="row justify-content-center my-5">
                        <div class="col-md-7 mt-3">
                            <section class="ca-logos slider">
                                <div class="slide">
                                    <a href="https://www.facebook.com/caalcc" target="_blank">
                                        <img src="<?= URL_ROOT; ?>/assets/images/cursos/logo_cc.png"
                                             title="Ciência da Computação">
                                    </a>
                                </div>
                                <div class="slide">
                                    <a href="https://www.facebook.com/caesufc" target="_blank">
                                        <img src="<?= URL_ROOT; ?>/assets/images/cursos/logo_software.png"
                                             title="Engenharia de Software">
                                    </a>
                                </div>
                                <div class="slide">
                                    <a href="https://www.facebook.com/Ufccamecrussas" target="_blank">
                                        <img src="<?= URL_ROOT; ?>/assets/images/cursos/logo_mec.png"
                                             title="Engenharia Mecânica">
                                    </a>
                                </div>
                                <div class="slide">
                                    <a href="https://www.facebook.com/caeproufcrussas/" target="_blank">
                                        <img src="<?= URL_ROOT; ?>/assets/images/cursos/logo_producao.png"
                                             title="Engenharia de Produção">
                                    </a>
                                </div>
                                <div class="slide"><img src="<?= URL_ROOT; ?>/assets/images/cursos/embreve.png"
                                                        title="Em breve o seu curso!"></div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer mt-0">
        <div class="text-center">
            <span class="align-middle">SISCARD &copy; 2019 — Desenvolvido por</span>
            <a href="https://www.includejr.com.br/" target="_blank">
                <img src="<?= URL_ROOT; ?>/assets/images/logos/include_logo.png" class="include_logo"/>
            </a>
        </div>
    </div>
<?php
require_once APP_ROOT . '/views/partials/pages/footer.php';
?>