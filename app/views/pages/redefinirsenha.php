<?php
require_once APP_ROOT . '/views/partials/pages/header.php';
?>
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth px-0">
            <div class="row w-100 mx-0">
                <div class="col-lg-7 mx-auto">
                    <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                        <div class="brand-logo">
                            <a href="<?= URL_ROOT; ?>">
                                <img src="<?= URL_ROOT; ?>/assets/images/logos/logo_siscard.png" alt="logo">
                            </a>
                        </div>
                        <h4>Esqueceu a senha?</h4>
                        <h6 class="font-weight-light">Sem problemas! Para recuperá-la, basta preencher o formulário
                            abaixo.</h6>

                        <?= flash('redefinirsenha'); ?>

                        <form action="<?= URL_ROOT; ?>/redefinirsenha" method="POST" class="pt-2">
                            <div class="form-group required">
                                <label class="control-label custom-label">Matrícula </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent border-right-0 <?php (!empty($formError['matricula'])) ? print 'is-invalid' : ''; ?>">
                                        <i class="ti-user text-primary"></i>
                                    </span>
                                    </div>
                                    <input type="text"
                                           <?php if (!empty($formData['matricula'])): ?>value="<?= $formData['matricula']; ?>"<?php endif; ?>
                                           name="matricula"
                                           class="form-control form-control-lg border-left-0 <?php (!empty($formError['matricula'])) ? print 'is-invalid' : ''; ?>"
                                           placeholder="Matrícula" autocomplete="off"/>
                                </div>
                                <?php if (!empty($formError['matricula'])): ?>
                                    <div class="text-invalid">
                                        <?= $formError['matricula']; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="form-group required">
                                <label class="control-label custom-label">CPF </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent border-right-0 <?php (!empty($formError['cpf'])) ? print 'is-invalid' : ''; ?>">
                                        <i class="ti-id-badge text-primary"></i>
                                    </span>
                                    </div>
                                    <input id="cpf" type="text" name="cpf"
                                           class="form-control form-control-lg border-left-0 <?php (!empty($formError['cpf'])) ? print 'is-invalid' : ''; ?>"
                                           placeholder="CPF"/>
                                </div>
                                <?php if (!empty($formError['cpf'])): ?>
                                    <div class="text-invalid">
                                        <?= $formError['cpf']; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-6 mb-2">
                                    <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">
                                        REDEFINIR SENHA
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <a href="<?= URL_ROOT; ?>"
                                       class="btn btn-block btn-light btn-lg font-weight-medium auth-form-btn">CANCELAR</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
    <div class="footer mt-0">
        <div class="text-center">
            <span class="align-middle">SISCARD &copy; 2019 — Desenvolvido por</span>
            <a href="https://www.includejr.com.br/" target="_blank">
                <img src="<?= URL_ROOT; ?>/assets/images/logos/include_logo.png" class="include_logo"/>
            </a>
        </div>
    </div>
<?php
require_once APP_ROOT . '/views/partials/pages/footer.php';
?>