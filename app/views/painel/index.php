<?php
require_once APP_ROOT . '/views/partials/painel/header.php';
?>
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-12 col-xl-6 mb-6 mb-xl-0 roboto-condensed">
                    <h4 class="font-weight-light">Olá, <strong
                                class="font-weight-bold"><?= $dadosAluno->nome; ?></strong>!</h4>
                    <h4 class="font-weight-light mb-0">Bem-vindo ao Siscard!</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Mensagens do CA</h4>
                    <div class="owl-carousel owl-theme full-width">
                        <?php if (count($dadosMensagens) == 0): ?>
                            <div class="item">
                                <div class="card text-white border-0">
                                    <div class="img-gradient">
                                        <img class="card-img" src="<?= URL_ROOT; ?>/assets/images/msgs/semnoticia.png"
                                             alt="">
                                    </div>
                                    <div class="card-img-overlay d-flex">
                                        <div class="mt-auto text-center w-100">
                                            <h3 class="slider-title">Sem novidades!</h3>
                                            <h6 class="card-text mb-4 font-weight-normal">
                                                O CA do seu curso ainda não cadastrou uma mensagem!
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php else: ?>
                            <?php foreach ($dadosMensagens as $mensagens): ?>
                                <div class="item">
                                    <div class="card text-white border-0">
                                        <div class="img-gradient">
                                            <img class="card-img" src="<?= $mensagens->imagem; ?>" alt="">
                                        </div>
                                        <div class="card-img-overlay d-flex">
                                            <div class="mt-auto text-center w-100">
                                                <h3 class="slider-title"><a
                                                            href="<?= URL_ROOT; ?>/painel/mensagem/<?= $mensagens->id; ?>"
                                                            class="mensagem-link"><?= $mensagens->titulo; ?></a></h3>
                                                <h6 class="card-text mb-4 font-weight-normal">
                                                    <?= resumirTexto($mensagens->mensagem, 50, FALSE); ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Seu Progresso</h4>
                    <ul class="bullet-line-list">
                        <?php foreach ($progressoaluno as $progresso): ?>
                            <li>
                                <h6><?= $progresso->titulo; ?></h6>
                                <p><?= $progresso->descricao; ?></p>
                                <p class="text-muted mb-4">
                                    <i class="ti-time"></i>
                                    <?= date('H:i - d/m/y', $progresso->dataregistro); ?>
                                </p>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Estabelecimentos com Descontos</h4>
                    <div class="owl-carousel owl-theme lazy-load">
                        <img class="owl-lazy" src="https://via.placeholder.com/260x162"
                             data-src="https://via.placeholder.com/260x162"
                             data-src-retina="https://via.placeholder.com/260x162" alt="image"/>
                        <img class="owl-lazy" src="https://via.placeholder.com/260x162"
                             data-src="https://via.placeholder.com/260x162"
                             data-src-retina="https://via.placeholder.com/260x162" alt="image"/>
                        <img class="owl-lazy" src="https://via.placeholder.com/260x162"
                             data-src="https://via.placeholder.com/260x162" alt="image"/>
                        <img class="owl-lazy" src="https://via.placeholder.com/260x162"
                             data-src="https://via.placeholder.com/260x162" alt="image"/>
                        <img class="owl-lazy" src="https://via.placeholder.com/260x162"
                             data-src="https://via.placeholder.com/260x162" alt="image"/>
                        <img class="owl-lazy" src="https://via.placeholder.com/260x162"
                             data-src="https://via.placeholder.com/260x162" alt="image"/>
                        <img class="owl-lazy" src="https://via.placeholder.com/260x162"
                             data-src="https://via.placeholder.com/260x162" alt="image"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php if ($boasVindas == TRUE): ?>
    <div class="modal fade" id="boasVindas">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-sm-8">
                                <h2 class="roboto-condensed mb-4">Que bom!</h2>
                                <p class="lead">
                                    O seu cadastro no Siscard foi realizado com sucesso!
                                    <br/>
                                    <br/>
                                    Em instantes você receberá um e-mail com informações do seu cadastro!
                                </p>
                            </div>
                            <div class="d-none d-sm-block col-sm-4">
                                <img src="<?= URL_ROOT; ?>/assets/images/msgs/aleluia.png" class="img-fluid"/>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-success" data-dismiss="modal">TUDO BEM!</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php
require_once APP_ROOT . '/views/partials/painel/footer.php';
?>