<?php
require_once APP_ROOT . '/views/partials/painel/header.php';
?>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title"><?= $dadosDaMensagem->titulo; ?></h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" aria-current="page">
                                <span class="breadcrumb-text">Painel de Controle</span>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Mensagem do CA</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="text-center">
                <img style="width: 60%;" src="<?= $dadosDaMensagem->imagem; ?>"/>
            </div>
            <br/>
            <p align="justify">
                <?= $dadosDaMensagem->mensagem; ?>
            </p>
            <div class="mt-4 text-muted small text-right">
                <span><i class="far fa-clock"></i> <?= date('d/m/y - H:i:s', $dadosDaMensagem->dataregistro) ?></span>
            </div>
        </div>
    </div>
<?php
require_once APP_ROOT . '/views/partials/painel/footer.php';
?>