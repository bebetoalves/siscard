<?php
require_once APP_ROOT . '/views/partials/painel/header.php';
?>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Minha Foto</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" aria-current="page">
                                <span class="breadcrumb-text">Painel de Controle</span>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Minha Foto</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

<?= flash('minhafoto'); ?>

    <div class="card">
        <div class="card-header">
            <span>Foto da Carteirinha</span>
        </div>
        <div class="card-body">
            <div class="row mt-2">
                <div class="col-md-3 text-center my-auto">
                    <?php if (empty($dadosFoto->foto)): ?>
                        <img src="<?= URL_ROOT; ?>/imagens/fotosalunos/avatar/170"
                             class="img-thumbnail img-responsive"/>
                    <?php else: ?>
                        <img src="<?= URL_ROOT; ?>/imagens/fotosalunos/<?= trim($dadosFoto->foto, '.jpg'); ?>/170"
                             class="img-thumbnail img-responsive"/>
                    <?php endif; ?>
                    <p class="mt-2 custom-label">
                        Status: <?= $statusFoto; ?>
                    </p>
                </div>
                <div class="col-md-9">
                    <ul class="list-ticked">
                        <h4 class="card-title">Informações Importantes!</h4>
                        <li>Só será aceito fotos com as extenções: png, jpeg e jpg.</li>
                        <li>Tire uma foto no formato 3x4 com fundo claro e orientação retrato.</li>
                        <li>O tamanho máximo da foto deverá ser 5MB.</li>
                    </ul>
                    <form action="<?= URL_ROOT; ?>/painel/minhafoto" method="POST" enctype="multipart/form-data">
                        <div class="form-group required mb-2">
                            <label class="control-label custom-label">Foto </label>
                            <input type="file" name="foto" class="file-upload-default">
                            <div class="input-group col-xs-12">
                                <input type="text"
                                       class="form-control file-upload-info <?php (!empty($formError)) ? print 'is-invalid' : ''; ?>"
                                       disabled="" placeholder="Selecione uma foto...">
                                <span class="input-group-append">
                                <button class="file-upload-browse btn btn-primary" type="button">SELECIONAR</button>
                            </span>
                                <?php if (!empty($formError)): ?>
                                    <div class="text-invalid">
                                        <?= $formError; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <button class="btn btn-primary btn-block" type="submit">ENVIAR</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="card mt-4">
        <div class="card-header">
            Exemplos de Fotos
        </div>
        <div class="card-body">
            <div class="row mt-3">
                <div class="col-md-6 text-center">
                    <img src="<?= URL_ROOT; ?>/assets/images/outras/foto_ok.png"
                         class="cartimage-exemplo img-fluid img-thumbnail"/>
                    <div class="custom-label mt-2 mb-3">
                        Foto correta!
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <img src="<?= URL_ROOT; ?>/assets/images/outras/foto_errada.png"
                         class="cartimage-exemplo img-fluid img-thumbnail"/>
                    <div class="custom-label mt-2 mb-3">
                        Foto inválida!
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
require_once APP_ROOT . '/views/partials/painel/footer.php';
?>