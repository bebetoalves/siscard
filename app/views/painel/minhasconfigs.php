<?php
require_once APP_ROOT . '/views/partials/painel/header.php';
?>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Configurações</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" aria-current="page">
                                <span class="breadcrumb-text">Painel de Controle</span>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Configurações</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

<?= flash('minhasconfigs'); ?>

    <div class="card">
        <div class="card-header">
            <span>Atualizar E-mail</span>
        </div>
        <div class="card-body">
            <form action="<?= URL_ROOT; ?>/painel/minhasconfigs" method="POST">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group required">
                            <label class="control-label custom-label">Novo E-mail </label>
                            <input type="text" name="email"
                                   value="<?= isset($formData['email']) ? $formData['email'] : ''; ?>"
                                   placeholder="Digite seu novo e-mail"
                                   class="form-control <?php (!empty($formError['email'])) ? print 'is-invalid' : ''; ?>"
                                   autocomplete="off"/>
                            <?php if (!empty($formError['email'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['email']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="operacao" value="1"/>
                <div class="row">
                    <div class="col-md-3">
                        <button class="btn btn-primary btn-block" type="submit">ATUALIZAR</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="my-5"></div>

    <div class="card">
        <div class="card-header">
            <span>Atualizar Senha</span>
        </div>
        <div class="card-body">
            <form action="<?= URL_ROOT; ?>/painel/minhasconfigs" method="POST">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group required">
                            <label class="control-label custom-label">Nova Senha </label>
                            <input type="password" name="senha" value="" placeholder="Digite sua nova senha"
                                   class="form-control <?php (!empty($formError['senha'])) ? print 'is-invalid' : ''; ?>"
                                   autocomplete="off"/>
                            <?php if (!empty($formError['senha'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['senha']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group required">
                            <label class="control-label custom-label">Confirme a Nova Senha </label>
                            <input type="password" name="csenha" value="" placeholder="Confirme sua nova senha"
                                   class="form-control <?php (!empty($formError['csenha'])) ? print 'is-invalid' : ''; ?>"
                                   autocomplete="off"/>
                            <?php if (!empty($formError['csenha'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['csenha']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="operacao" value="2"/>
                <div class="row">
                    <div class="col-md-3">
                        <button class="btn btn-primary btn-block" type="submit">ATUALIZAR</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php
require_once APP_ROOT . '/views/partials/painel/footer.php';
?>