<?php
require_once APP_ROOT . '/views/partials/painel/header.php';
?>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Minhas Informações</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" aria-current="page">
                                <span class="breadcrumb-text">Painel de Controle</span>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Minhas Informações</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
<?= flash('atualMessages'); ?>
    <div class="card mb-5">
        <div class="card-header">
            <span>Atualizar Informações</span>
        </div>
        <div class="card-body">
            <form action="<?= URL_ROOT; ?>/painel/minhasinfos" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label custom-label">Nome Completo </label>
                            <input type="text" name="nome"
                                   value="<?php empty($formData['nome']) ? print $dadosAluno->nome : print $formData['nome']; ?>"
                                   placeholder="Nome Completo"
                                   class="form-control <?php (!empty($formError['nome'])) ? print 'is-invalid' : ''; ?>"
                                   autocomplete="off"/>
                            <?php if (!empty($formError['nome'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['nome']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label custom-label">Matrícula </label>
                            <input type="text" value="<?= $dadosAluno->matricula; ?>" placeholder="N° da matrícula"
                                   class="form-control" disabled/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group required">
                            <label class="control-label custom-label">Nascimento </label>
                            <input type="text" name="nascimento"
                                   value="<?php empty($formData['nascimento']) ? print $dadosAluno->nascimento : print $formData['nascimento']; ?>"
                                   placeholder="DD/MM/AAAA"
                                   class="form-control <?php (!empty($formError['nascimento'])) ? print 'is-invalid' : ''; ?>"
                                   id="nascimento" autocomplete="off"/>
                            <?php if (!empty($formError['nascimento'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['nascimento']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group required">
                            <label class="control-label custom-label">CPF </label>
                            <input type="text" name="cpf"
                                   value="<?php empty($formData['cpf']) ? print($dadosAluno->cpf == 0 ? '' : $dadosAluno->cpf) : print $formData['cpf']; ?>"
                                   placeholder="Apenas dígitos"
                                   class="form-control <?php (!empty($formError['cpf'])) ? print 'is-invalid' : ''; ?>"
                                   autocomplete="off"/>
                            <?php if (!empty($formError['cpf'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['cpf']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group required">
                            <label class="control-label custom-label">RG </label>
                            <input type="text" name="rg"
                                   value="<?php empty($formData['rg']) ? print($dadosAluno->rg == 0 ? '' : $dadosAluno->rg) : print $formData['rg']; ?>"
                                   placeholder="Apenas dígitos"
                                   class="form-control <?php (!empty($formError['rg'])) ? print 'is-invalid' : ''; ?>"
                                   autocomplete="off"/>
                            <?php if (!empty($formError['rg'])): ?>
                                <div class="text-invalid">
                                    <?= $formError['rg']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-primary btn-block">ATUALIZAR</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php
require_once APP_ROOT . '/views/partials/painel/footer.php';
?>