<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $tituloDaPagina . ' | ' . SITE_NAME; ?></title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= URL_ROOT; ?>/assets/vendors/iconfonts/ti-icons/css/themify-icons.css">
    <link rel="stylesheet" href="<?= URL_ROOT; ?>/assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?= URL_ROOT; ?>/assets/vendors/css/vendor.bundle.addons.css">
    <link rel="stylesheet" href="<?= URL_ROOT; ?>/assets/vendors/css/bootstrap4-toggle.min.css">
    <link rel="stylesheet" href="<?= URL_ROOT; ?>/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= URL_ROOT; ?>/assets/css/style.css">
    <link rel="shortcut icon" href="<?= URL_ROOT; ?>/assets/images/logos/favicon.png"/>
</head>

<body class="boxed-layout">
<div class="container-scroller">
    <!-- navbar -->
    <?php
    require_once 'navbar.php';
    ?>
    <div class="container-fluid page-body-wrapper">
        <!-- sidebar -->
        <?php
        require_once 'sidebar.php';
        ?>
        <!-- inicio do painel -->
        <div class="main-panel">
            <div class="content-wrapper">
                        