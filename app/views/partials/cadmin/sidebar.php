<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <div class="user-detalhes text-center">
        <?php if (empty($meusDados->foto)): ?>
            <img src="<?= URL_ROOT; ?>/assets/images/faces/avatar.png" class="foto-sidebar"/>
        <?php else: ?>
            <img src="<?= URL_ROOT . '/uploads/ca/' . $meusDados->foto; ?>" class="foto-sidebar"/>
        <?php endif; ?>
        <p class="nome-usuario mt-3">
            <?= $meusDados->nome; ?>
            <br/>
            <span class="nome-status"><i class="fas fa-user-tie"></i> Moderador</span>
        </p>
    </div>
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="<?= URL_ROOT; ?>/capainel/index">
                <i class="ti-home menu-icon"></i>
                <span class="menu-title">Início</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= URL_ROOT; ?>/capainel/geriralunos">
                <i class="ti-view-list-alt menu-icon"></i>
                <span class="menu-title">Gerir Alunos </span>
                <?php if ($totalFotosParaValidar > 0): ?>
                    <div class="badge badge-pill badge-danger">
                        +<?= $totalFotosParaValidar; ?>
                    </div>
                <?php endif; ?>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= URL_ROOT; ?>/capainel/carteirinhas">
                <i class="ti-id-badge menu-icon"></i>
                <span class="menu-title">Carteirinhas </span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= URL_ROOT; ?>/capainel/configs">
                <i class="ti-settings menu-icon"></i>
                <span class="menu-title">Configurações</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= URL_ROOT; ?>/capainel/sair">
                <i class="fas fa-sign-out-alt menu-icon"></i>
                <span class="menu-title">Sair</span>
            </a>
        </li>
    </ul>
</nav>