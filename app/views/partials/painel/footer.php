</div>
<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">SISCARD - Sistema de Carteiras Estudantis &copy; 2019.</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Desenvolvido por
                            <a href="https://www.includejr.com.br/" target="_blank">
                            <img src="<?= URL_ROOT; ?>/assets/images/logos/include_logo.png" class="include_logo"/></a>
                            </span>
    </div>
</footer>
</div>
<!-- fim do painel -->
</div>
</div>

<script src="<?= URL_ROOT; ?>/assets/vendors/js/vendor.bundle.base.js"></script>
<script src="<?= URL_ROOT; ?>/assets/vendors/js/vendor.bundle.addons.js"></script>
<script src="<?= URL_ROOT; ?>/assets/js/off-canvas.js"></script>
<script src="<?= URL_ROOT; ?>/assets/js/template.js"></script>
<script src="<?= URL_ROOT; ?>/assets/js/settings.js"></script>
<script src="<?= URL_ROOT; ?>/assets/js/todolist.js"></script>
<script src="<?= URL_ROOT; ?>/assets/js/jquery.mask.js"></script>
<script src="<?= URL_ROOT; ?>/assets/js/file-upload.js"></script>
<script src="<?= URL_ROOT; ?>/assets/js/owl-carousel.js"></script>
<script src="<?= URL_ROOT; ?>/assets/js/slick.min.js"></script>
<script src="<?= URL_ROOT; ?>/assets/js/main.js"></script>

<script>
    $(window).on('load', function () {
        $('#boasVindas').modal('show');
    });
</script>
</body>

</html>