<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo mr-5" href="<?= URL_ROOT; ?>/painel/index">
            <img src="<?= URL_ROOT; ?>/assets/images/logos/logo_siscard.png" class="mr-2"/>
        </a>
        <a class="navbar-brand brand-logo-mini" href="<?= URL_ROOT; ?>/painel/index">
            <img src="<?= URL_ROOT; ?>/assets/images/logos/logo-mini.png" alt="logo"/>
        </a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item nav-profile dropdown">
            </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                data-toggle="offcanvas">
            <i class="fas fa-bars"></i>
        </button>
    </div>
</nav>