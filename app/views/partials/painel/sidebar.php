<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <div class="user-detalhes text-center">
        <?php if (empty($dadosFoto->foto)): ?>
            <img src="<?= URL_ROOT; ?>/imagens/fotosalunos/avatar" class="foto-sidebar"/>
        <?php else: ?>
            <img src="<?= URL_ROOT; ?>/imagens/fotosalunos/<?= trim($dadosFoto->foto, '.jpg'); ?>"
                 class="foto-sidebar"/>
        <?php endif; ?>
        <p class="nome-usuario mt-3">
            <?= tratarNomeCompleto($dadosAluno->nome); ?>
            <br/>
            <span class="nome-status"><i class="fas fa-user-graduate"></i> Aluno</span>
        </p>
    </div>
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="<?= URL_ROOT; ?>/painel/index">
                <i class="ti-home menu-icon"></i>
                <span class="menu-title">Início</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= URL_ROOT; ?>/painel/minhasinfos">
                <i class="ti-user menu-icon"></i>
                <span class="menu-title">Minhas Informações</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= URL_ROOT; ?>/painel/minhafoto">
                <i class="ti-gallery menu-icon"></i>
                <span class="menu-title">Minha Foto</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= URL_ROOT; ?>/painel/minhasconfigs">
                <i class="ti-settings menu-icon"></i>
                <span class="menu-title">Configurações</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= URL_ROOT; ?>/painel/sair">
                <i class="fas fa-sign-out-alt menu-icon"></i>
                <span class="menu-title">Sair</span>
            </a>
        </li>
    </ul>
</nav>