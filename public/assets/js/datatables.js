(function($) {
  'use strict';
  $(function() {
    $('#lotes-gerados').DataTable({
      "aLengthMenu": [
        [5, 10, 15, -1],
        [5, 10, 15, "Tudo"]
      ],
      "iDisplayLength": 10,
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json",
        search: ""
      },
      "order": [[ 1, "desc" ]]
    });
    $('#gerir-alunos').DataTable({
      "aLengthMenu": [
        [5, 10, 15, -1],
        [5, 10, 15, "Tudo"]
      ],
      "iDisplayLength": 10,
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json",
        search: ""
      },
      "order": [[ 0, "desc" ]]
    });
  });
})(jQuery);