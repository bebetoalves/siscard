$(document).ready(function () {
    $('#nascimento').mask('00/00/0000');
    $('#matricula').mask('000000');
    $('#cpf').mask('00000000000', {'translation': {0: {pattern: /[0-9]/}}});
    $('#rg').mask('00000000000000', {'translation': {0: {pattern: /[0-9]/}}});
    $('.ca-logos').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
        dots: false,
        pauseOnHover: true,
        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 2
                }
            }]
    });

    $('#acaofoto').on('change', function () {
        if (this.value === 'off')
        {
            $("#motivo").show();
        } else
        {
            $("#motivo").hide();
        }
    });
    
    $('#acaopagamento').on('change', function() {
        setTimeout(function () {
            $('#formpagamento').submit();
        }, 400);
    });
    
   $('.image-link').magnificPopup({type:'image'});

   $('#imprimirLote').on("click", function () {
    $('#imprimir').printThis({
        importCSS: true,
        canvas: true,
    });
  });
});
